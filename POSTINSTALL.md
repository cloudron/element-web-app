Matrix consists of two components - backend and frontend. This app only provides a web frontend
for Matrix. If you haven't done so already, please install the [Synapse](/#/appstore/org.matrix.synapse)
Matrix backend (home server) in a subdomain like `matrix.$CLOUDRON-APP-DOMAIN`.

This app is pre-configured to use the matrix installation at `matrix.$CLOUDRON-APP-DOMAIN`.
If you installed Synapse at another location, use the [Web Terminal](https://cloudron.io/documentation/apps/#web-terminal)
to edit `/app/data/config.json` to set the home server.

