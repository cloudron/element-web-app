[0.1.0]
* Initial version

[0.2.0]
* Update description and screenshots

[0.3.0]
* Use release tarballs instead of building code
* Upgrade riot to 1.5.7

[0.4.0]
* Update riot to 1.5.15
* Setup config.json with proper defaults

[1.0.0]
* Update base image to 2.0.0

[1.1.0]
* Update riot to 1.6.0
* Cross-signing and E2EE by default for DMs and private rooms enabled
* Upgrade to React SDK 2.5.0 and JS SDK 6.0.0

[1.1.1]
* Update riot to 1.6.1
* [Full changelog](https://github.com/vector-im/riot-web/releases/tag/v1.6.1)
* Upgrade to React SDK 2.6.0 and JS SDK 6.1.0

[1.1.2]
* Update riot to 1.6.2
* [Full changelog](https://github.com/vector-im/riot-web/releases/tag/v1.6.2)
* Upgrade to React SDK 2.6.1

[1.1.3]
* Update riot to 1.6.3
* [Full changelog](https://github.com/vector-im/riot-web/releases/tag/v1.6.3)
* Fixes a vulnerability in single sign-on (SSO) deployments

[1.1.4]
* Update riot to 1.6.4
* [Full changelog](https://github.com/vector-im/riot-web/releases/tag/v1.6.4)

[1.2.0]
* Remove matrix.org welcome bot - https://github.com/vector-im/riot-web/pull/12894

[1.2.1]
* Update riot to 1.6.5
* [Full changelog](https://github.com/vector-im/riot-web/releases/tag/v1.6.5)
* Upgrade to JS SDK 6.2.2 and React SDK 2.7.2

[1.2.2]
* Update riot to 1.6.6
* [Full changelog](https://github.com/vector-im/riot-web/releases/tag/v1.6.6)
* Upgrade to JS SDK 7.0.0 and React SDK 2.8.0

[1.3.0]
* Update riot to 1.6.7
* [Full changelog](https://github.com/vector-im/riot-web/releases/tag/v1.6.7)
* Upgrade to React SDK 2.8.1

[1.3.1]
* Update riot to 1.6.8
* [Full changelog](https://github.com/vector-im/riot-web/releases/tag/v1.6.8)
* Upgrade to JS SDK 7.1.0 and React SDK 2.9.0

[1.4.0]
* Update Element to 1.7.0
* [Full changelog](https://github.com/vector-im/riot-web/releases/tag/v1.7.0)
* App name changed from Riot to Element

[1.4.1]
* Update Element to 1.7.1
* [Full changelog](https://github.com/vector-im/riot-web/releases/tag/v1.7.1)
* Run pngcrush on vector-icons
* Use the right protocol for SSO URLs
* Fix mstile-310x150 by renaming it

[1.4.2]
* Update Element to 1.7.2
* [Full changelog](https://github.com/vector-im/riot-web/releases/tag/v1.7.2)
* Upgrade to React SDK 3.0.0 and JS SDK 8.0.0
* Capitalize letters #14566
* Riot to Element #14581

[1.4.3]
* Update Element to 1.7.3
* [Full changelog](https://github.com/vector-im/riot-web/releases/tag/v1.7.3)
* Element Web 1.7.3 fixes an issue where replying to a specially formatted message would make it seem like the replier said something they did not. Thanks to Sorunome for responsibly disclosing this via Matrix's Security Disclosure Policy.
Element Web 1.7.3 fixes an issue where an unexpected language ID in a code block could cause Element to crash. Thanks to SakiiR for responsibly disclosing this via Matrix's Security Disclosure Policy.
* Upgrade to React SDK 3.1.0 and JS SDK 8.0.1

[1.4.4]
* Update Element to 1.7.4
* [Full changelog](https://github.com/vector-im/riot-web/releases/tag/v1.7.4)
* Upgrade to React SDK 3.2.0 and JS SDK 8.1.0

[1.4.5]
* Update Element to 1.7.5
* [Full changelog](https://github.com/vector-im/riot-web/releases/tag/v1.7.5)
* Element Web 1.7.5 fixes an issue where encrypted state events could break incoming call handling.

[1.4.6]
* Update Element to 1.7.6
* [Full changelog](https://github.com/vector-im/riot-web/releases/tag/v1.7.7)
* Upgrade to React SDK 3.4.1

[1.4.7]
* Update Element to 1.7.9
* [Full changelog](https://github.com/vector-im/riot-web/releases/tag/v1.7.9)
* Upgrade to React SDK 3.6.0 and JS SDK 8.5.0
* Add `/app/data/custom` as a location for custom assets

[1.4.8]
* Update Element to 1.7.10
* [Full changelog](https://github.com/vector-im/riot-web/releases/tag/v1.7.10)
* Adjust for new widget messaging APIs #15497
* Upgrade to React SDK 3.6.1

[1.4.9]
* Update Element to 1.7.11
* [Full changelog](https://github.com/vector-im/riot-web/releases/tag/v1.7.11)
* Upgrade to React SDK 3.7.0 and JS SDK 9.0.0

[1.4.10]
* Update Element to 1.7.12
* [Full changelog](https://github.com/vector-im/riot-web/releases/tag/v1.7.12)

[1.4.11]
* Update Element to 1.7.13
* [Full changelog](https://github.com/vector-im/riot-web/releases/tag/v1.7.13)
* Upgrade to React SDK 3.8.0 and JS SDK 9.1.0

[1.4.12]
* Update Element to 1.7.14
* [Full changelog](https://github.com/vector-im/riot-web/releases/tag/v1.7.14)
* Upgrade to React SDK 3.9.0 and JS SDK 9.2.0

[1.4.13]
* Update Element to 1.7.15
* [Full changelog](https://github.com/vector-im/riot-web/releases/tag/v1.7.15)
* Upgrade to React SDK 3.10.0 and JS SDK 9.3.0

[1.4.14]
* Update Element to 1.7.16
* [Full changelog](https://github.com/vector-im/riot-web/releases/tag/v1.7.16)

[1.4.15]
* Update Element to 1.7.17
* [Full changelog](https://github.com/vector-im/riot-web/releases/tag/v1.7.17)
* Upgrade to React SDK 3.12.0 and JS SDK 9.5.0

[1.4.16]
* Update Element to 1.7.18
* [Full changelog](https://github.com/vector-im/riot-web/releases/tag/v1.7.18)

[1.4.17]
* Update Element to 1.7.19
* [Full changelog](https://github.com/vector-im/riot-web/releases/tag/v1.7.19)

[1.5.0]
* Update Element to 1.7.20
* [Full changelog](https://github.com/vector-im/riot-web/releases/tag/v1.7.20)
* Use base image v3

[1.5.1]
* Update Element to 1.7.21
* [Full changelog](https://github.com/vector-im/riot-web/releases/tag/v1.7.21)
* Upgrade to React SDK 3.14.0 and JS SDK 9.7.0

[1.5.2]
* Update Element to 1.7.22
* [Full changelog](https://github.com/vector-im/riot-web/releases/tag/v1.7.22)
* Fixes a severity issue (CVE-2021-21320) where the user content sandbox can be abused to trick users into opening unexpected documents

[1.5.3]
* Update Element to 1.7.24
* [Full changelog](https://github.com/vector-im/riot-web/releases/tag/v1.7.24)

[1.5.4]
* Update Element to 1.7.25

[1.5.5]
* Update Element to 1.7.26

[1.5.6]
* Update Element to 1.7.27

[1.5.7]
* Update Element to 1.7.28

[1.5.8]
* Update Element to 1.7.29

[1.5.9]
* Update Element to 1.7.30
* Upgrade to React SDK 3.23.0 and JS SDK 11.2.0

[1.5.10]
* Update Element to 1.7.31
* Upgrade to React SDK 3.24.0 and JS SDK 12.0.0

[1.5.11]
* Update Element to 1.7.32

[1.5.12]
* Set "branding" to Element

[1.5.13]
* Update Element to 1.7.33

[1.5.14]
* Update Element to 1.7.34

[1.6.0]
* Update Element to 1.8.0
* [Full changelog](https://github.com/vector-im/element-web/releases/tag/v1.8.0)
* Show how long a call was on call tiles (#6570). Fixes #18405. Contributed by SimonBrandner.
* Add regional indicators to emoji picker (#6490). Fixes #14963. Contributed by robintown.
* Make call control buttons accessible to screen reader users (#6181). Fixes #18358. Contributed by pvagner.
* Skip sending a thumbnail if it is not a sufficient saving over the original (#6559). Fixes #17906.
* Increase PiP snapping speed (#6539). Fixes #18371. Contributed by SimonBrandner.
* Improve and move the incoming call toast (#6470). Fixes #17912. Contributed by SimonBrandner.
* Allow all of the URL schemes that Firefox allows (#6457). Contributed by aaronraimist.
* Improve bubble layout colors (#6452). Fixes #18081. Contributed by SimonBrandner.
* Spaces let users switch between Home and All Rooms behaviours (#6497). Fixes #18093.
* Support for MSC2285 (hidden read receipts) (#6390). Contributed by SimonBrandner.
* Group pinned message events with MELS (#6349). Fixes #17938. Contributed by SimonBrandner.
* Make version copiable (#6227). Fixes #17603 and #18329. Contributed by SimonBrandner.

[1.6.1]
* Update Element to 1.8.1
* [Full changelog](https://github.com/vector-im/element-web/releases/tag/v1.8.1)
* Fix multiple VoIP regressions (matrix-org/matrix-js-sdk#1860).

[1.6.2]
* Update Element to 1.8.2
* [Full changelog](https://github.com/vector-im/element-web/releases/tag/v1.8.2)
* Add a warning on E2EE rooms if you try to make them public (#5698). Contributed by SimonBrandner.
* Allow pagination of the space hierarchy and use new APIs (#6507). Fixes #18089 and #18427.
* Improve emoji in composer (#6650). Fixes #18593 and #18593. Contributed by SimonBrandner.
* Allow playback of replied-to voice message (#6629). Fixes #18599 and #18599. Contributed by SimonBrandner.
* Format autocomplete suggestions vertically (#6620). Fixes #17574 and #17574. Contributed by SimonBrandner.
* Remember last MemberList search query per-room (#6640). Fixes #18613 and #18613. Contributed by SimonBrandner.

[1.6.3]
* Update Element to 1.8.4
* [Full changelog](https://github.com/vector-im/element-web/releases/tag/v1.8.4)
* Fix a security issue with message key sharing. See https://matrix.org/blog/2021/09/13/vulnerability-disclosure-key-sharing for details.

[1.6.4]
* Update Element to 1.8.5
* [Full changelog](https://github.com/vector-im/element-web/releases/tag/v1.8.5)
* Add bubble highlight styling (#6582). Fixes #18295 and #18295. Contributed by SimonBrandner.
* Create narrow mode for Composer (#6682). Fixes #18533 and #18533.
* Prefer matrix.to alias links over room id in spaces & share (#6745). Fixes #18796 and #18796.
* Stop automatic playback of voice messages if a non-voice message is encountered (#6728). Fixes #18850 and #18850.
* Show call length during a call (#6700). Fixes #18566 and #18566. Contributed by SimonBrandner.
* Serialize and retry mass-leave when leaving space (#6737). Fixes #18789 and #18789.
* Improve form handling in and around space creation (#6739). Fixes #18775 and #18775.
* Split autoplay GIFs and videos into different settings (#6726). Fixes #5771 and #5771. Contributed by SimonBrandner.
* Add autoplay for voice messages (#6710). Fixes #18804, #18715, #18714 #17961 and #18804.
* Allow to use basic html to format invite messages (#6703). Fixes #15738 and #15738. Contributed by skolmer.

[1.7.0]
* Update Element to 1.9.0
* [Full changelog](https://github.com/vector-im/element-web/releases/tag/v1.9.0)

[1.7.1]
* Update Element to 1.9.1
* [Full changelog](https://github.com/vector-im/element-web/releases/tag/v1.9.1)
* Decrease profile button touch target (#6900). Contributed by ColonisationCaptain.
* Don't let click events propagate out of context menus (#6892).
* Allow closing Dropdown via its chevron (#6885). Fixes #19030 and #19030.
* Improve AUX panel behaviour (#6699). Fixes #18787 and #18787. Contributed by SimonBrandner.
* A nicer opening animation for the Image View (#6454). Fixes #18186 and #18186. Contributed by SimonBrandner.

[1.7.2]
* Update Element to 1.9.2
* [Full changelog](https://github.com/vector-im/element-web/releases/tag/v1.9.2)
* Upgrade to matrix-js-sdk#14.0.1

[1.7.3]
* Update Element to 1.9.3
* [Full changelog](https://github.com/vector-im/element-web/releases/tag/v1.9.3)
* Convert the "Cryptography" settings panel to an HTML table to assist screen reader users. (#6968). Contributed by andybalaam.
* Swap order of private space creation and tweak copy (#6967). Fixes #18768 and #18768.
* Add spacing to Room settings - Notifications subsection (#6962). Contributed by CicadaCinema.
* Use HTML tables for some tabular user interface areas, to assist with screen reader use (#6955). Contributed by andybalaam.
* Fix space invite edge cases (#6884). Fixes #19010 #17345 and #19010.
* Allow options to cascade kicks/bans throughout spaces (#6829). Fixes #18969 and #18969.
* Hide kick & ban options in UserInfo when looking at own profile (#6911). Fixes #19066 and #19066.

[1.7.4]
* Update Element to 1.9.4

[1.7.5]
* Update Element to 1.9.5
* [Full changelog](https://github.com/vector-im/element-web/releases/tag/v1.9.5)

[1.7.6]
* Update Element to 1.9.6
* [Full changelog](https://github.com/vector-im/element-web/releases/tag/v1.9.6)

[1.7.7]
* Update Element to 1.9.7
* [Full changelog](https://github.com/vector-im/element-web/releases/tag/v1.9.7)

[1.7.8]
* Update Element to 1.9.8
* [Full changelog](https://github.com/vector-im/element-web/releases/tag/v1.9.8)

[1.7.9]
* Update Element to 1.9.9
* [Full changelog](https://github.com/vector-im/element-web/releases/tag/v1.9.9)
* Add permission dropdown for sending reactions (#7492). Fixes #20450.
* Ship maximised widgets and remove feature flag (#7509).
* Properly maintain aspect ratio of inline images (#7503).
* Add zoom buttons to the location view (#7482).
* Remove bubble from around location events (#7459). Fixes #20323.
* Disable "Publish this room" option in invite only rooms (#7441). Fixes #6596. Contributed by @aaronraimist.

[1.7.10]
* Update Element to 1.10.0
* [Full changelog](https://github.com/vector-im/element-web/releases/tag/v1.10.0)

[1.7.11]
* Update Element to 1.10.1
* [Full changelog](https://github.com/vector-im/element-web/releases/tag/v1.10.1)
* Fix the sticker picker (#7692). Fixes vector-im/element-web#20797.
* Ensure UserInfo can be rendered without a room (#7687). Fixes vector-im/element-web#20830.
* Fix publishing address wrongly demanding the alias be available (#7690). Fixes vector-im/element-web#12013 and vector-im/element-web#20833.

[1.7.12]
* Update Element to 1.10.2
* [Full changelog](https://github.com/vector-im/element-web/releases/tag/v1.10.2)

[1.7.13]
* Update Element to 1.10.3
* [Full changelog](https://github.com/vector-im/element-web/releases/tag/v1.10.3)
* Add map tile URL for location sharing maps to sample config

[1.7.14]
* Update Element to 1.10.4
* [Full changelog](https://github.com/vector-im/element-web/releases/tag/v1.10.4)
* Fix bug where badge colour on encrypted rooms may not be correct until anothe rmessage is sent

[1.7.15]
* Update Element to 1.10.5
* [Full changelog](https://github.com/vector-im/element-web/releases/tag/v1.10.5)

[1.7.16]
* Update Element to 1.10.6
* [Full changelog](https://github.com/vector-im/element-web/releases/tag/v1.10.6)
* Fix some crashes in the right panel

[1.7.17]
* Update Element to 1.10.7
* [Full changelog](https://github.com/vector-im/element-web/releases/tag/v1.10.7)
* Security: Fix a bug where URL previews could be enabled in right panel timelines when they should not have been.
* Add unexposed account setting for hiding poll creation (#7972).
* Allow pinning polls (#7922). Fixes #20152.
* Make trailing : into a setting (#6711). Fixes #16682. Contributed by @SimonBrandner.
* Location sharing > back button (#7958).
* use LocationAssetType (#7965).

[1.7.18]
* Update Element to 1.10.8
* [Full changelog](https://github.com/vector-im/element-web/releases/tag/v1.10.8)

[1.7.19]
* Update Element to 1.10.9
* [Full changelog](https://github.com/vector-im/element-web/releases/tag/v1.10.9)
* Release threads as a beta feature (#8081). Fixes #21351.
* More video rooms design updates (#8222).
* Update video rooms to new design specs (#8207). Fixes #21515, #21516 #21519 and #21526.
* Live Location Sharing - left panel warning with error (#8201).
* Live location sharing - Stop publishing location to beacons with consecutive errors (#8194).
* Live location sharing: allow retry when stop sharing fails (#8193).
* Allow voice messages to be scrubbed in the timeline (#8079). Fixes #18713.

[1.7.20]
* Update Element to 1.10.10
* [Full changelog](https://github.com/vector-im/element-web/releases/tag/v1.10.10)
* Fixes around threads beta in degraded mode (#8319). Fixes #21762.

[1.7.21]
* Update Element to 1.10.11
* [Full changelog](https://github.com/vector-im/element-web/releases/tag/v1.10.11)
* Handle forced disconnects from Jitsi (#21697). Fixes #21517.
* Improve performance of switching to rooms with lots of servers and ACLs (#8347).
* Avoid a reflow when setting caret position on an empty composer (#8348).
* Add message right-click context menu as a labs feature (#5672).
* Live location sharing - basic maximised beacon map (#8310).
* Live location sharing - render users own beacons in timeline (#8296).
* Improve Threads beta around degraded mode (#8318).
* Live location sharing - beacon in timeline happy path (#8285).
* Add copy button to View Source screen (#8278). Fixes #21482. Contributed by @olivialivia.
* Add heart effect (#6188). Contributed by @CicadaCinema.
* Update new room icon (#8239).

[1.7.22]
* Update Element to 1.10.12
* [Full changelog](https://github.com/vector-im/element-web/releases/tag/v1.10.12)
* Made the location map change the cursor to a pointer so it looks like it's clickable (https (#8451). Fixes #21991. Contributed by @Odyssey346.
* Implement improved spacing for the thread list and timeline (#8337). Fixes #21759. Contributed by @luixxiul.
* LLS: expose way to enable live sharing labs flag from location dialog (#8416).
* Fix source text boxes in View Source modal should have full width (#8425). Fixes #21938. Contributed by @EECvision.
* Read Receipts: never show +1, if it’s just 4, show all of them (#8428). Fixes #21935.
* Add opt-in analytics to onboarding tasks (#8409). Fixes #21705.

[1.7.23]
* Update Element to 1.10.13
* [Full changelog](https://github.com/vector-im/element-web/releases/tag/v1.10.13)
* Lots of fixes and new features!

[1.7.24]
* Update Element to 1.10.14
* [Full changelog](https://github.com/vector-im/element-web/releases/tag/v1.10.14)
* Make Lao translation available (#22358). Fixes #22327.
* Configure custom home.html via .well-known/matrix/client["io.element.embedded_pages"]["home_url"] for all your element-web/desktop users (#7790). Contributed by @johannes-krude.
* Live location sharing - open location in OpenStreetMap (#8695). Contributed by @kerryarchibald.
* Show a dialog when Jitsi encounters an error (#8701). Fixes #22284.
* Add support for setting the avatar_url of widgets by integration managers. (#8550). Contributed by @Fox32.
* Add an option to ignore (block) a user when reporting their events (#8471).
* Add the option to disable hardware acceleration (#8655). Contributed by @novocaine.
* Slightly better presentation of read receipts to screen reader users (#8662). Fixes #22293. Contributed by @pvagner.
* Add jump to related event context menu item (#6775). Fixes #19883.
* Add public room directory hook (#8626).

[1.7.25]
* Update Element to 1.10.15
* [Full changelog](https://github.com/vector-im/element-web/releases/tag/v1.10.15)
* Fix missing element desktop preferences (#8798). Contributed by @t3chguy.

[1.8.0]
* Update Element to 1.11.0
* [Full changelog](https://github.com/vector-im/element-web/releases/tag/v1.11.0)
* Remove Piwik support (#8835).
* Document how to configure a custom home.html. (#21066). Contributed by @johannes-krude.
* Move New Search Experience out of beta (#8859). Contributed by @justjanne.
* Switch video rooms to spotlight layout when in PiP mode (#8912). Fixes #22574.
* Live location sharing - render message deleted tile for redacted beacons (#8905). Contributed by @kerryarchibald.
* Improve view source dialog style (#8883). Fixes #22636. Contributed by @luixxiul.
* Improve integration manager dialog style (#8888). Fixes #22642. Contributed by @luixxiul.
* Implement MSC3827: Filtering of /publicRooms by room type (#8866). Fixes #22578.
* Show chat panel when opening a video room with unread messages (#8812). Fixes #22527.
* Live location share - forward latest location (#8860). Contributed by @kerryarchibald.
* Allow integration managers to validate user identity after opening (#8782). Contributed by @Half-Shot.
* Create a common header on right panel cards on BaseCard (#8808). Contributed by @luixxiul.
* Integrate searching public rooms and people into the new search experience (#8707). Fixes #21354 and #19349. Contributed by @justjanne.
* Bring back waveform for voice messages and retain seeking (#8843). Fixes #21904.
* Improve colors in settings (#7283).
* Keep draft in composer when a slash command syntax errors (#8811). Fixes #22384.
* Release video rooms as a beta feature (#8431).
* Clarify logout key backup warning dialog. Contributed by @notramo. (#8741). Fixes #15565. Contributed by @MadLittleMods.
* Slightly improve the look of the Message edits dialog (#8763). Fixes #22410.
* Add support for MD / HTML in room topics (#8215). Fixes #5180. Contributed by @Johennes.
* Live location share - link to timeline tile from share warning (#8752). Contributed by @kerryarchibald.
* Improve composer visiblity (#8578). Fixes #22072 and #17362.
* Makes the avatar of the user menu non-draggable (#8765). Contributed by @luixxiul.
* Improve widget buttons behaviour and layout (#8734).
* Use AccessibleButton for 'Reset All' link button on SetupEncryptionBody (#8730). Contributed by @luixxiul.
* Adjust message timestamp position on TimelineCard in non-bubble layouts (#8745). Fixes #22426. Contributed by @luixxiul.
* Use AccessibleButton for 'In reply to' link button on ReplyChain (#8726). Fixes #22407. Contributed by @luixxiul.
* Live location share - enable reply and react to tiles (#8721). Contributed by @kerryarchibald.
* Change dash to em dash issues fixed (#8455). Fixes #21895. Contributed by @goelesha.

[1.8.1]
* Update Element to 1.11.1
* [Full changelog](https://github.com/vector-im/element-web/releases/tag/v1.11.1)
* Enable URL tooltips on hover for Element Desktop (#22286). Fixes undefined/element-web#6532.
* Hide screenshare button in video rooms on Desktop (#9045).
* Add a developer command to reset Megolm and Olm sessions (#9044).
* add spaces to TileErrorBoundary (#9012). Contributed by @HarHarLinks.
* Location sharing - add localised strings to map (#9025). Fixes #21443. Contributed by @kerryarchibald.
* Added trim to ignore whitespaces in email check (#9027). Contributed by @ankur12-1610.
* Improve `_GenericEventListSummary.scss` (#9005). Contributed by @luixxiul.
* Live location share - tiles without tile server (PSG-591) (#8962). Contributed by @kerryarchibald.
* Add option to display tooltip on link hover (#8394). Fixes #21907.
* Support a module API surface for custom functionality (#8246).
* Adjust encryption copy when creating a video room (#8989). Fixes #22737.
* Add bidirectonal isolation for pills (#8985). Contributed by @sha-265.
* Delabs Show current avatar and name for users in message history (#8764). Fixes #22336.
* Live location share - open latest location in map site (#8981). Contributed by @kerryarchibald.
* Improve LinkPreviewWidget (#8881). Fixes #22634. Contributed by @luixxiul.
* Render HTML topics in rooms on space home (#8939).
* Hide timestamp on event tiles being edited on every layout (#8956). Contributed by @luixxiul.
* Introduce new copy icon (#8942).
* Allow finding group DMs by members in spotlight (#8922). Fixes #22564. Contributed by @justjanne.
* Live location share - explicitly stop beacons replaced beacons (#8933). Contributed by @kerryarchibald.
* Remove unpin from widget kebab menu (#8924).
* Live location share - redact related locations on beacon redaction (#8926). Contributed by @kerryarchibald.
* Live location share - disallow message pinning (#8928). Contributed by @kerryarchibald.

[1.8.2]
* Update Element to 1.11.2
* [Full changelog](https://github.com/vector-im/element-web/releases/tag/v1.11.1)
* Enable URL tooltips on hover for Element Desktop (#22286). Fixes undefined/element-web#6532.
* Hide screenshare button in video rooms on Desktop (#9045).
* Add a developer command to reset Megolm and Olm sessions (#9044).
* add spaces to TileErrorBoundary (#9012). Contributed by @HarHarLinks.
* Location sharing - add localised strings to map (#9025). Fixes #21443. Contributed by @kerryarchibald.

[1.8.3]
* Update Element to 1.11.3
* [Full changelog](https://github.com/vector-im/element-web/releases/tag/v1.11.3)
* Improve auth aria attributes and semantics (#22948).
* Device manager - New device tile info design (#9122). Contributed by @kerryarchibald.
* Device manager generic settings subsection component (#9147). Contributed by @kerryarchibald.
* Migrate the hidden read receipts flag to new "send read receipts" option (#9141).
* Live location sharing - share location at most every 5 seconds (#9148). Contributed by @kerryarchibald.
* Increase max length of voice messages to 15m (#9133). Fixes #18620.
* Move pin drop out of labs (#9135).

[1.8.4]
* Update Element to 1.11.4
* [Full changelog](https://github.com/vector-im/element-web/releases/tag/v1.11.4)
* Add super cool feature (#9222). Contributed by @gefgu.
* Make use of js-sdk roomNameGenerator to handle i18n for generated room names (#9209). Fixes #21369.
* Fix progress bar regression throughout the app (#9219). Fixes #23121.
* Reuse empty string & space string logic for event types in devtools (#9218). Fixes #23115.
* Reduce amount of requests done by the onboarding task list (#9194). Fixes #23085. Contributed by @justjanne.
* Avoid hardcoding branding in user onboarding (#9206). Fixes #23111. Contributed by @justjanne.
* End jitsi call when member is banned (#8879). Contributed by @maheichyk.
* Fix context menu being opened when clicking message action bar buttons (#9200). Fixes #22279 and #23100.
* Add gap between checkbox and text in report dialog following the same pattern (8px) used in the gap between the two buttons. It fixes #23060 (#9195). Contributed by @gefgu.
* Fix url preview AXE and layout issue & add percy test (#9189). Fixes #23083.
* Wrap long space names (#9201). Fixes #23095.
* Attempt to fix Failed to execute 'removeChild' on 'Node' (#9196).
* Fix soft crash around space hierarchy changing between spaces (#9191). Fixes matrix-org/element-web-rageshakes#14613.
* Fix soft crash around room view store metrics (#9190). Fixes matrix-org/element-web-rageshakes#14361.
* Fix the same person appearing multiple times when searching for them. (#9177). Fixes #22851.
* Fix space panel subspace indentation going missing (#9167). Fixes #23049.
* Fix invisible power levels tile when showing hidden events (#9162). Fixes #23013.
* Space panel accessibility improvements (#9157). Fixes #22995.
* Fix inverted logic for showing UserWelcomeTop component (#9164). Fixes #23037.

[1.8.5]
* Update Element to 1.11.5
* [Full changelog](https://github.com/vector-im/element-web/releases/tag/v1.11.5)
* Device manager - hide unverified security recommendation when only current session is unverified (#9228). Contributed by @kerryarchibald.
* Device manager - scroll to filtered list from security recommendations (#9227). Contributed by @kerryarchibald.
* Device manager - updated dropdown style in filtered device list (#9226). Contributed by @kerryarchibald.
* Device manager - device type and verification icons on device tile (#9197). Contributed by @kerryarchibald.
* Description of DM room with more than two other people is now being displayed correctly (#9231). Fixes #23094.
* Fix voice messages with multiple composers (#9208). Fixes #23023. Contributed by @grimhilt.
* Fix suggested rooms going missing (#9236). Fixes #23190.
* Fix tooltip infinitely recursing (#9235). Fixes matrix-org/element-web-rageshakes#15107, matrix-org/element-web-rageshakes#15093 matrix-org/element-web-rageshakes#15092 and matrix-org/element-web-rageshakes#15077.
* Fix plain text export saving (#9230). Contributed by @jryans.
* Add missing space in SecurityRoomSettingsTab (#9222). Contributed by @gefgu.
* Make use of js-sdk roomNameGenerator to handle i18n for generated room names (#9209). Fixes #21369.
* Fix progress bar regression throughout the app (#9219). Fixes #23121.
* Reuse empty string & space string logic for event types in devtools (#9218). Fixes #23115.

[1.8.6]
* Update ELement to 1.11.6
* [Full changelog](https://github.com/vector-im/element-web/releases/tag/v1.11.6)
* Element Call video rooms (#9267).
* Device manager - rename session (#9282).
* Allow widgets to read related events (#9210). Contributed by @dhenneke.
* Device manager - logout of other session (#9280).
* Device manager - logout current session (#9275).
* Device manager - verify other devices (#9274).
* Allow integration managers to remove users (#9211).
* Device manager - add verify current session button (#9252).
* Add NotifPanel dot back. (#9242). Fixes #17641.
* Implement MSC3575: Sliding Sync (#8328).
* Add the clipboard read permission for widgets (#9250). Contributed by @stefanmuhle.

[1.8.7]
* Update Element to 1.11.7
* [Full changelog](https://github.com/vector-im/element-web/releases/tag/v1.11.7)
* Fix for CVE-2022-39249
* Fix for CVE-2022-39250
* Fix for CVE-2022-39251
* Fix for CVE-2022-39236

[1.8.8]
* Update Element to 1.11.8
* [Full changelog](https://github.com/vector-im/element-web/releases/tag/v1.11.8)
* Bump IDB crypto store version (#2705).

[1.8.9]
* Update Element to 1.11.9
* [Full changelog](https://github.com/vector-im/element-web/releases/tag/v1.11.9)
* Legacy Piwik config.json option piwik.policy_url is deprecated in favour of privacy_policy_url. Support will be removed in the next release.
* Device manager - select all devices (#9330). Contributed by @kerryarchibald.
* New group call experience: Call tiles (#9332).
* Add Shift key to FormatQuote keyboard shortcut (#9298). Contributed by @owi92.
* Device manager - sign out of multiple sessions (#9325). Contributed by @kerryarchibald.
* Display push toggle for web sessions (MSC3890) (#9327).
* Add device notifications enabled switch (#9324).
* Implement push notification toggle in device detail (#9308).
* New group call experience: Starting and ending calls (#9318).
* New group call experience: Room header call buttons (#9311).
* Make device ID copyable in device list (#9297). Contributed by @duxovni.
* Use display name instead of user ID when rendering power events (#9295).
* Read receipts for threads (#9239). Fixes #23191.
* Use the correct sender key when checking shared secret (#2730). Fixes #23374.
* Fix device selection in pre-join screen for Element Call video rooms (#9321). Fixes #23331.
* Don't render a 1px high room topic if the room topic is empty (#9317). Contributed by @Arnei.
* Don't show feedback prompts when that UIFeature is disabled (#9305). Fixes #23327.
* Fix soft crash around unknown room pills (#9301). Fixes matrix-org/element-web-rageshakes#15465.
* Fix spaces feedback prompt wrongly showing when feedback is disabled (#9302). Fixes #23314.
* Fix tile soft crash in ReplyInThreadButton (#9300). Fixes matrix-org/element-web-rageshakes#15493.

[1.8.10]
* Update Element to 1.11.10
* [Full changelog](https://github.com/vector-im/element-web/releases/tag/v1.11.10)
* Use correct default for notification silencing (#9388). Fixes vector-im/element-web#23456.

[1.8.11]
* Update Element to 1.11.11
* [Full changelog](https://github.com/vector-im/element-web/releases/tag/v1.11.11)
* Device manager - tweak string formatting of default device name (#23457).
* Add Element Call participant limit (#23431).
* Add Element Call brand (#23443).
* Include a file-safe room name and ISO date in chat exports (#9440). Fixes #21812 and #19724.
* Room call banner (#9378). Fixes #23453. Contributed by @toger5.

[1.8.12]
* Update Element to 1.11.12
* [Full changelog](https://github.com/vector-im/element-web/releases/tag/v1.11.12)
* Fix config.json failing to load for Jitsi wrapper in non-root deployment (#23577).

[1.8.13]
* Update Element to 1.11.13
* [Full changelog](https://github.com/vector-im/element-web/releases/tag/v1.11.13)
* Fix default behavior of Room.getBlacklistUnverifiedDevices (#2830). Contributed by @duxovni.
* Catch server versions API call exception when starting the client (#2828). Fixes #23634.
* Fix authedRequest including Authorization: Bearer undefined for password resets (#2822). Fixes #23655.

[1.8.14]
* Update Element to 1.11.14
* [Full changelog](https://github.com/vector-im/element-web/releases/tag/v1.11.14)
* Loading threads with server-side assistance (#9356). Fixes #21807, #21799, #21911, #22141, #22157, #22641, #22501 #22438 and #21678. Contributed by @justjanne.
* Make thread replies trigger a room list re-ordering (#9510). Fixes #21700.
* Device manager - add extra details to device security and renaming (#9501). Contributed by @kerryarchibald.
* Add plain text mode to the wysiwyg composer (#9503). Contributed by @florianduros.
* Sliding Sync: improve sort order, show subspace rooms, better tombstoned room handling (#9484).
* Device manager - add learn more popups to filtered sessions section (#9497). Contributed by @kerryarchibald.
* Show thread notification if thread timeline is closed (#9495). Fixes #23589.
* Add message editing to wysiwyg composer (#9488). Contributed by @florianduros.
* Device manager - confirm sign out of other sessions (#9487). Contributed by @kerryarchibald.
* Automatically request logs from other users in a call when submitting logs (#9492).
* Add thread notification with server assistance (MSC3773) (#9400). Fixes #21114, #21413, #21416, #21433, #21481, #21798, #21823 #23192 and #21765.
* Support for login + E2EE set up with QR (#9403). Contributed by @hughns.
* Allow pressing Enter to send messages in new composer (#9451). Contributed by @andybalaam.

[1.8.15]
* Update Element to 1.11.15
* [Full changelog](https://github.com/vector-im/element-web/releases/tag/v1.11.15)
* Make clear notifications work with threads (#9575). Fixes #23751.
* Change "None" to "Off" in notification options (#9539). Contributed by @Arnei.
* Advanced audio processing settings (#8759). Fixes #6278. Contributed by @MrAnno.
* Add way to create a user notice via config.json (#9559).
* Improve design of the rich text editor (#9533). Contributed by @florianduros.
* Enable user to zoom beyond image size (#5949). Contributed by @jaiwanth-v.
* Fix: Move "Leave Space" option to the bottom of space context menu (#9535). Contributed by @hanadi92.

[1.8.16]
* Update Element to 1.11.16
* Update Cloudron base image to 4.0.0
* [Full changelog](https://github.com/vector-im/element-web/releases/tag/v1.11.16)
* Further improve replies (#6396). Fixes #19074, #18194 #18027 and #19179.
* Enable users to join group calls from multiple devices (#9625).
* fix(visual): make cursor a pointer for summaries (#9419). Contributed by @r00ster91.
* Add placeholder for rich text editor (#9613).
* Consolidate public room search experience (#9605). Fixes #22846.
* New password reset flow (#9581). Fixes #23131.
* Device manager - add tooltip to device details toggle (#9594).
* sliding sync: add lazy-loading member support (#9530).
* Limit formatting bar offset to top of composer (#9365). Fixes #12359. Contributed by @owi92.
* Fix issues around up arrow event edit shortcut (#9645). Fixes #18497 and #18964.
* Fix search not being cleared when clicking on a result (#9635). Fixes #23845.
* Fix screensharing in 1:1 calls (#9612). Fixes #23808.
* Fix the background color flashing when joining a call (#9640).
* Fix the size of the 'Private space' icon (#9638).
* Fix reply editing in rich text editor (https (#9615).
* Fix thread list jumping back down while scrolling (#9606). Fixes #23727.
* Fix regression with TimelinePanel props updates not taking effect (#9608). Fixes #23794.
* Fix form tooltip positioning (#9598). Fixes #22861.
* Extract Search handling from RoomView into its own Component (#9574). Fixes #498.
* Fix call splitbrains when switching between rooms (#9692).
* [Backport staging] Fix replies to emotes not showing as inline (#9708).

[1.8.17]
* Update Element to 1.11.17
* [Full changelog](https://github.com/vector-im/element-web/releases/tag/v1.11.17)
* Add inline code formatting to rich text editor (#9720).
* Add emoji handling for plain text mode of the new rich text editor (#9727).
* Overlay virtual room call events into main timeline (#9626). Fixes #22929.
* Adds a new section under "Room Settings" > "Roles & Permissions" which adds the possibility to multiselect users from this room and grant them more permissions. (#9596). Contributed by @GoodGuyMarco.
* Add emoji handling for rich text mode (#9661).
* Add setting to hide bold notifications (#9705).
* Further password reset flow enhancements (#9662).
* Snooze the bulk unverified sessions reminder on dismiss (#9706).

[1.8.18]
* Update Element to 1.11.18
* [Full changelog](https://github.com/vector-im/element-web/releases/tag/v1.11.18)
* Switch threads on for everyone (#9879).
* Make threads use new Unable to Decrypt UI (#9876). Fixes #24060.
* Add edit and remove actions to link in RTE [Labs] (#9864).
* Remove extensible events v1 experimental rendering (#9881).
* Make create poll dialog scale better (PSG-929) (#9873). Fixes #21855.
* Change RTE mode icons (#9861).
* Device manager - prune client information events after remote sign out (#9874).
* Check connection before starting broadcast (#9857).
* Enable sent receipt for poll start events (PSG-962) (#9870).
* Change clear notifications to have more readable copy (#9867).
* combine search results when the query is present in multiple successive messages (#9855). Fixes #3977. Contributed by @grimhilt.
* Disable bubbles for broadcasts (#9860). Fixes #24140.
* Enable reactions and replies for broadcasts (#9856). Fixes #24042.
* Improve switching between rich and plain editing modes (#9776).
* Redesign the picture-in-picture window (#9800). Fixes #23980.
* User on-boarding tasks now appear in a static order. (#9799). Contributed by @GoodGuyMarco.
* Device manager - contextual menus (#9832).
* If listening a non-live broadcast and changing the room, the broadcast will be paused (#9825). Fixes #24078.
* Consider own broadcasts from other device as a playback (#9821). Fixes #24068.
* Add link creation to rich text editor (#9775).
* Add mark as read option in room setting (#9798). Fixes #24053.
* Device manager - current device design and copy tweaks (#9801).
* Unify notifications panel event design (#9754).
* Add actions for integration manager to send and read certain events (#9740).
* Device manager - design tweaks (#9768).
* Change room list sorting to activity and unread first by default (#9773). Fixes #24014.
* Add a config flag to enable the rust crypto-sdk (#9759).
* Improve decryption error UI by consolidating error messages and providing instructions when possible (#9544). Contributed by @duxovni.
* Honor font settings in Element Call (#9751). Fixes #23661.
* Device manager - use deleteAccountData to prune device manager client information events (#9734).

[1.8.19]
* Update Element to 1.11.19
* [Full changelog](https://github.com/vector-im/element-web/releases/tag/v1.11.19)
* fix crash on browsers that don't support Array.at (#9935). Contributed by @andybalaam.

[1.8.20]
* Update Element to 1.11.20
* [Full changelog](https://github.com/vector-im/element-web/releases/tag/v1.11.20)
* (Part 2) of prevent crash on older browsers (replace .at() with array.length-1)

[1.8.21]
* Update Element to 1.11.21
* [Full changelog](https://github.com/vector-im/element-web/releases/tag/v1.11.21)
* Move pin drop out of labs (#22993).
* Quotes for rich text editor (RTE) (#9932). Contributed by @alunturner.
* Show the room name in the room header during calls (#9942). Fixes #24268.
* Add code blocks to rich text editor (#9921). Contributed by @alunturner.
* Add new style for inline code (#9936). Contributed by @florianduros.
* Add disabled button state to rich text editor (#9930). Contributed by @alunturner.
* Change the rageshake "app" for auto-rageshakes (#9909).
* Device manager - tweak settings display (#9905). Contributed by @kerryarchibald.
* Add list functionality to rich text editor (#9871). Contributed by @alunturner.

[1.8.22]
* Update Element to 1.11.22
* [Full changelog](https://github.com/vector-im/element-web/releases/tag/v1.11.22)
* Bump version number to fix problems upgrading from v1.11.21-rc.1a

[1.8.23]
* Update Element to 1.11.23
* [Full changelog](https://github.com/vector-im/element-web/releases/tag/v1.11.23)
* Description of QR code sign in labs feature (#23513). Contributed by @hughns.
* Add option to find own location in map views (#10083).
* Render poll end events in timeline (#10027). Contributed by @kerryarchibald.
* Indicate unread messages in tab title (#10096). Contributed by @tnt7864.
* Open message in editing mode when keyboard up is pressed (RTE) (#10079). Contributed by @florianduros.

[1.8.24]
* Update Element to 1.11.24
* [Full changelog](https://github.com/vector-im/element-web/releases/tag/v1.11.24)
* Remove experimental PWA support for Firefox and Safari (#24630).
* Fix block code styling in rich text editor (#10246). Contributed by @alunturner.
* Poll history: fetch more poll history (#10235). Contributed by @kerryarchibald.
* Sort short/exact emoji matches before longer incomplete matches (#10212). Fixes #23210. Contributed by @grimhilt.
* Poll history: detail screen (#10172). Contributed by @kerryarchibald.
* Provide a more detailed error message than "No known servers" (#6048). Fixes #13247. Contributed by @aaronraimist.
* Say when a call was answered from a different device (#10224).
* Widget permissions customizations using module api (#10121). Contributed by @maheichyk.
* Fix copy button icon overlapping with copyable text (#10227). Contributed by @Adesh-Pandey.
* Support joining non-peekable rooms via the module API (#10154). Contributed by @maheichyk.
* The "new login" toast does now display the same device information as in the settings. "No" does now open the device settings. "Yes, it was me" dismisses the toast. (#10200).
* Do not prompt for a password when doing a „reset all“ after login (#10208).
* Display "The sender has blocked you from receiving this message" error message instead of "Unable to decrypt message" (#10202). Contributed by @florianduros.
* Polls: show warning about undecryptable relations (#10179). Contributed by @kerryarchibald.
* Poll history: fetch last 30 days of polls (#10157). Contributed by @kerryarchibald.
* Poll history - ended polls list items (#10119). Contributed by @kerryarchibald.
* Remove threads labs flag and the ability to disable threads (#9878). Fixes #24365.
* Show a success dialog after setting up the key backup (#10177). Fixes #24487.
* Release Sign in with QR out of labs (#10182). Contributed by @hughns.
* Hide indent button in rte (#10149). Contributed by @alunturner.
* Add option to find own location in map views (#10083).
* Render poll end events in timeline (#10027). Contributed by @kerryarchibald.

[1.8.25]
* Update Element to 1.11.25
* [Full changelog](https://github.com/vector-im/element-web/releases/tag/v1.11.25)
* Remove experimental PWA support for Firefox and Safari (#24630).
* Only allow to start a DM with one email if encryption by default is enabled (#10253). Fixes #23133.
* DM rooms are now encrypted if encryption by default is enabled and only inviting a single email address. Any action in the result DM room will be blocked until the other has joined. (#10229).
* Reduce bottom margin of ReplyChain on compact modern layout (#8972). Fixes #22748. Contributed by @luixxiul.
* Support for v2 of MSC3903 (#10165). Contributed by @hughns.
* When starting a DM, existing rooms with pending third-party invites will be reused. (#10256). Fixes #23139.
* Polls push rules: synchronise poll rules with message rules (#10263). Contributed by @kerryarchibald.
* New verification request toast button labels (#10259).
* Remove padding around integration manager iframe (#10148).
* Fix block code styling in rich text editor (#10246). Contributed by @alunturner.
* Poll history: fetch more poll history (#10235). Contributed by @kerryarchibald.
* Sort short/exact emoji matches before longer incomplete matches (#10212). Fixes #23210. Contributed by @grimhilt.
* Poll history: detail screen (#10172). Contributed by @kerryarchibald.
* Provide a more detailed error message than "No known servers" (#6048). Fixes #13247. Contributed by @aaronraimist.
* Say when a call was answered from a different device (#10224).
* Widget permissions customizations using module api (#10121). Contributed by @maheichyk.
* Fix copy button icon overlapping with copyable text (#10227). Contributed by @Adesh-Pandey.
* Support joining non-peekable rooms via the module API (#10154). Contributed by @maheichyk.
* The "new login" toast does now display the same device information as in the settings. "No" does now open the device settings. "Yes, it was me" dismisses the toast. (#10200).
* Do not prompt for a password when doing a „reset all“ after login (#10208).

[1.8.26]
* Update Element to 1.11.26
* [Full changelog](https://github.com/vector-im/element-web/releases/tag/v1.11.26)
* Changes for matrix-js-sdk v24.0.0
* Changes for matrix-react-sdk v3.69.0

[1.8.27]
* Update Element to 1.11.27
* [Full changelog](https://github.com/vector-im/element-web/releases/tag/v1.11.27)
* Fix detection of encryption for all users in a room (#10487). Fixes #24995.

[1.8.28]
* Update Element to 1.11.28
* [Full changelog](https://github.com/vector-im/element-web/releases/tag/v1.11.28)
* No changes, version bumped to sync with element-desktop.

[1.8.29]
* Update Element to 1.11.29
* [Full changelog](https://github.com/vector-im/element-web/releases/tag/v1.11.29)
* Allow desktop app to expose recent rooms in UI integrations (#16940).
* Add API params to mute audio and/or video in Jitsi calls by default (#24820). Contributed by @dhenneke.
* Style mentions as pills in rich text editor (#10448). Contributed by @alunturner.
* Show room create icon if "UIComponent.roomCreation" is enabled (#10364). Contributed by @maheichyk.
* Mentions as links rte (#10463). Contributed by @alunturner.
* Better error handling in jump to date (#10405). Contributed by @MadLittleMods.
* Show "Invite" menu option if "UIComponent.sendInvites" is enabled. (#10363). Contributed by @maheichyk.
* Added UserProfilesStore, LruCache and user permalink profile caching (#10425). Fixes #10559.
* Mentions as links rte (#10422). Contributed by @alunturner.
* Implement MSC3952: intentional mentions (#9983).
* Implement MSC3973: Search users in the user directory with the Widget API (#10269). Contributed by @dhenneke.
* Permalinks to message are now displayed as pills (#10392). Fixes #24751 and #24706.
* Show search,dial,explore in filterContainer if "UIComponent.filterContainer" is enabled (#10381). Contributed by @maheichyk.
* Increase space panel collapse clickable area (#6084). Fixes #17379. Contributed by @jaiwanth-v.
* Add fallback for replies to Polls (#10380). Fixes #24197. Contributed by @kerryarchibald.
* Permalinks to rooms and users are now pillified (#10388). Fixes #24825.
* Poll history - access poll history from room settings (#10356). Contributed by @kerryarchibald.
* Add API params to mute audio and/or video in Jitsi calls by default (#10376). Contributed by @dhenneke.
* Notifications: inline error message on notifications saving error (#10288). Contributed by @kerryarchibald.
* Support dynamic room predecessor in SpaceProvider (#10348). Contributed by @andybalaam.
* Support dynamic room predecessors for RoomProvider (#10346). Contributed by @andybalaam.
* Support dynamic room predecessors in OwnBeaconStore (#10339). Contributed by @andybalaam.
* Support dynamic room predecessors in ForwardDialog (#10344). Contributed by @andybalaam.
* Support dynamic room predecessors in SpaceHierarchy (#10341). Contributed by @andybalaam.
* Support dynamic room predecessors in AddExistingToSpaceDialog (#10342). Contributed by @andybalaam.
* Support dynamic room predecessors in leave-behaviour (#10340). Contributed by @andybalaam.
* Support dynamic room predecessors in StopGapWidgetDriver (#10338). Contributed by @andybalaam.
* Support dynamic room predecessors in WidgetLayoutStore (#10326). Contributed by @andybalaam.
* Support dynamic room predecessors in SpaceStore (#10332). Contributed by @andybalaam.
* Sync polls push rules on changes to account_data (#10287). Contributed by @kerryarchibald.
* Support dynamic room predecessors in BreadcrumbsStore (#10295). Contributed by @andybalaam.
* Improved a11y for Field feedback and Secure Phrase input (#10320). Contributed by @Sebbones.
* Support dynamic room predecessors in RoomNotificationStateStore (#10297). Contributed by @andybalaam.

[1.8.30]
* Update Element to 1.11.30
* [Full changelog](https://github.com/vector-im/element-web/releases/tag/v1.11.30)
* Fixes for CVE-2023-30609 / GHSA-xv83-x443-7rmw
* Pick sensible default option for phone country dropdown (#10627). Fixes #3528.
* Relate field validation tooltip via aria-describedby (#10522). Fixes #24963.
* Handle more completion types in rte autocomplete (#10560). Contributed by @alunturner.
* Show a tile for an unloaded predecessor room if it has via_servers (#10483). Contributed by @andybalaam.
* Exclude message timestamps from aria live region (#10584). Fixes #5696.
* Make composer format bar an aria toolbar (#10583). Fixes #11283.
* Improve accessibility of font slider (#10473). Fixes #20168 and #24962.
* fix file size display from kB to KB (#10561). Fixes #24866. Contributed by @NSV1991.
* Handle /me in rte (#10558). Contributed by @alunturner.
* bind html with switch for manage extension setting option (#10553). Contributed by @NSV1991.
* Handle command completions in RTE (#10521). Contributed by @alunturner.
* Add room and user avatars to rte (#10497). Contributed by @alunturner.
* Support for MSC3882 revision 1 (#10443). Contributed by @hughns.
* Check profiles before starting a DM (#10472). Fixes #24830.
* Quick settings: Change the copy / labels on the options (#10427). Fixes #24522. Contributed by @justjanne.
* Update rte autocomplete styling (#10503). Contributed by @alunturner.

[1.8.31]
* Update Element to 1.11.31
* [Full changelog](https://github.com/vector-im/element-web/releases/tag/v1.11.31)
* Improve Content-Security-Policy (#25210).
* Add UIFeature.locationSharing to hide location sharing (#10727).
* Memoize field validation results (#10714).
* Commands for plain text editor (#10567). Contributed by @alunturner.
* Allow 16 lines of text in the rich text editors (#10670). Contributed by @alunturner.
* Bail out of RoomSettingsDialog when room is not found (#10662). Contributed by @kerryarchibald.
* Element-R: Populate device list for right-panel (#10671). Contributed by @florianduros.
* Make existing and new issue URLs configurable (#10710). Fixes #24424.
* Fix usages of ARIA tabpanel (#10628). Fixes #25016.
* Element-R: Starting a DMs with a user (#10673). Contributed by @florianduros.
* ARIA Accessibility improvements (#10675).
* ARIA Accessibility improvements (#10674).
* Add arrow key controls to emoji and reaction pickers (#10637). Fixes #17189.
* Translate credits in help about section (#10676).
* Fix: reveal images when image previews are disabled (#10781). Fixes #25271. Contributed by @kerryarchibald.
* Fix autocomplete not resetting properly on message send (#10741). Fixes #25170.
* Fix start_sso not working with guests disabled (#10720). Fixes #16624.
* Fix soft crash with Element call widgets (#10684).
* Send correct receipt when marking a room as read (#10730). Fixes #25207.
* Offload some more waveform processing onto a worker (#9223). Fixes #19756.
* Consolidate login errors (#10722). Fixes #17520.
* Fix all rooms search generating permalinks to wrong room id (#10625). Fixes #25115.
* Posthog properly handle Analytics ID changing from under us (#10702). Fixes #25187.
* Fix Clock being read as an absolute time rather than duration (#10706). Fixes #22582.
* Properly translate errors in ChangePassword.tsx so they show up translated to the user but not in our logs (#10615). Fixes #9597. Contributed by @MadLittleMods.
* Honour feature toggles in guest mode (#10651). Fixes #24513. Contributed by @andybalaam.
* Fix default content in devtools event sender (#10699). Contributed by @tulir.
* Fix a crash when a call ends while you're in it (#10681). Fixes #25153.
* Fix lack of screen reader indication when triggering auto complete (#10664). Fixes #11011.
* Fix typing tile duplicating users (#10678). Fixes #25165.
* Fix wrong room topic tooltip position (#10667). Fixes #25158.
* Fix create subspace dialog not working (#10652). Fixes #24882.

[1.8.32]
* Update Element to 1.11.32
* [Full changelog](https://github.com/vector-im/element-web/releases/tag/v1.11.32)
* Redirect to the SSO page if sso_redirect_options.on_welcome_page is enabled and the URL hash is empty (#25495). Contributed by @dhenneke.
* vector/index.html: Allow fetching blob urls (#25336). Contributed by @SuperKenVery.
* When joining room in sub-space join the parents too (#11011).
* Include thread replies in message previews (#10631). Fixes #23920.
* Use semantic headings in space preferences (#11021). Contributed by @kerryarchibald.
* Use semantic headings in user settings - Ignored users (#11006). Contributed by @kerryarchibald.
* Use semantic headings in user settings - profile (#10973). Fixes #25461. Contributed by @kerryarchibald.
* Use semantic headings in user settings - account (#10972). Contributed by @kerryarchibald.
* Support Insert from iPhone or iPad in Safari (#10851). Fixes #25327. Contributed by @SuperKenVery.
* Specify supportedStages for User Interactive Auth (#10975). Fixes #19605.
* Pass device id to widgets (#10209). Contributed by @Fox32.
* Use semantic headings in user settings - discovery (#10838). Contributed by @kerryarchibald.
* Use semantic headings in user settings - Notifications (#10948). Contributed by @kerryarchibald.
* Use semantic headings in user settings - spellcheck and language (#10959). Contributed by @kerryarchibald.
* Use semantic headings in user settings Appearance (#10827). Contributed by @kerryarchibald.
* Use semantic heading in user settings Sidebar & Voip (#10782). Contributed by @kerryarchibald.
* Use semantic headings in user settings Security (#10774). Contributed by @kerryarchibald.
* Use semantic headings in user settings - integrations and account deletion (#10837). Fixes #25378. Contributed by @kerryarchibald.
* Use semantic headings in user settings Preferences (#10794). Contributed by @kerryarchibald.
* Use semantic headings in user settings Keyboard (#10793). Contributed by @kerryarchibald.
* RTE plain text mentions as pills (#10852). Contributed by @alunturner.
* Allow welcome.html logo to be replaced by config (#25339). Fixes #8636.
* Use semantic headings in user settings Labs (#10773). Contributed by @kerryarchibald.
* Use semantic list elements for menu lists and tab lists (#10902). Fixes #24928.
* Fix aria-required-children axe violation (#10900). Fixes #25342.
* Enable pagination for overlay timelines (#10757). Fixes vector-im/voip-internal#107.
* Add tooltip to disabled invite button due to lack of permissions (#10869). Fixes #9824.
* Respect configured auth_header_logo_url for default Welcome page (#10870).
* Specify lazy loading for avatars (#10866). Fixes #1983.
* Room and user mentions for plain text editor (#10665). Contributed by @alunturner.
* Add audible notifcation on broadcast error (#10654). Fixes #25132.
* Fall back from server generated thumbnail to original image (#10853).
* Use semantically correct elements for room sublist context menu (#10831). Fixes vector-im/customer-retainer#46.
* Avoid calling prepareToEncrypt onKeyDown (#10828).
* Allows search to recognize full room links (#8275). Contributed by @bolu-tife.
* "Show rooms with unread messages first" should not be on by default for new users (#10820). Fixes #25304. Contributed by @kerryarchibald.
* Fix emitter handler leak in ThreadView (#10803).
* Add better error for email invites without identity server (#10739). Fixes #16893.
* Move reaction message previews out of labs (#10601). Fixes #25083.
* Sort muted rooms to the bottom of their section of the room list (#10592). Fixes #25131. Contributed by @kerryarchibald.
* Use semantic headings in user settings Help & About (#10752). Contributed by @kerryarchibald.
* use ExternalLink components for external links (#10758). Contributed by @kerryarchibald.
* Use semantic headings in space settings (#10751). Contributed by @kerryarchibald.
* Use semantic headings for room settings content (#10734). Contributed by @kerryarchibald.

[1.8.33]
* Update Element to 1.11.33
* [Full changelog](https://github.com/vector-im/element-web/releases/tag/v1.11.33)
* Bump matrix-react-sdk to v3.73.1 for matrix-js-sdk v26.0.1. Fixes #25526.

[1.8.34]
* Update Element to 1.11.34
* [Full changelog](https://github.com/vector-im/element-web/releases/tag/v1.11.34)
* OIDC: add delegatedauthentication to validated server config (#11053). Contributed by @kerryarchibald.
* Allow image pasting in plain mode in RTE (#11056). Contributed by @alunturner.
* Show room options menu if "UIComponent.roomOptionsMenu" is enabled (#10365). Contributed by @maheichyk.
* Allow image pasting in rich text mode in RTE (#11049). Contributed by @alunturner.
* Update voice broadcast redaction to use MSC3912 with_rel_type instead of with_relations (#11014). Fixes #25471.
* Add config to skip widget_build_url for DM rooms (#11044). Fixes vector-im/customer-retainer#74.
* Inhibit interactions on forward dialog message previews (#11025). Fixes #23459.
* Removed DecryptionFailureBar.tsx (#11027). Fixes vector-im/element-meta#1358. Contributed by @florianduros.
* Fix translucent TextualEvent on search results panel (#10810). Fixes #25292. Contributed by @luixxiul.
* Matrix matrix scheme permalink constructor not stripping query params (#11060). Fixes #25535.
* Fix: "manually verify by text" does nothing (#11059). Fixes #25375. Contributed by @kerryarchibald.
* Make group calls respect the ICE fallback setting (#11047). Fixes vector-im/voip-internal#65.
* Align list items on the tooltip to the start (#11041). Fixes #25355. Contributed by @luixxiul.
* Clear thread panel event permalink when changing rooms (#11024). Fixes #25484.
* Fix spinner placement on pinned widgets being reloaded (#10970). Fixes #25431. Contributed by @luixxiul.

[1.8.35]
* Update Element to 1.11.35
* [Full changelog](https://github.com/vector-im/element-web/releases/tag/v1.11.35)
* Don't setup keys on login when encryption is force disabled (#11125). Contributed by @kerryarchibald.
* OIDC: attempt dynamic client registration (#11074). Fixes #25468 and #25467. Contributed by @kerryarchibald.
* OIDC: Check static client registration and add login flow (#11088). Fixes #25467. Contributed by @kerryarchibald.
* Improve message body output from plain text editor (#11124). Contributed by @alunturner.
* Disable encryption toggle in room settings when force disabled (#11122). Contributed by @kerryarchibald.
* Add .well-known config option to force disable encryption on room creation (#11120). Contributed by @kerryarchibald.
* Handle permalinks in room topic (#11115). Fixes #23395.
* Add at room avatar for RTE (#11106). Contributed by @alunturner.
* Remove new room breadcrumbs (#11104).

[1.8.36]
* Update Element to 1.11.36
* [Full changelog](https://github.com/vector-im/element-web/releases/tag/v1.11.36)
* Fixes for CVE-2023-37259 / GHSA-c9vx-2g7w-rp65
* Deprecate customisations in favour of Module API (#25736). Fixes #25733.
* OIDC: store initial screen in session storage (#25688). Fixes #25656. Contributed by @kerryarchibald.
* Allow default_server_config as a fallback config (#25682). Contributed by @ShadowRZ.
* OIDC: remove auth params from url after login attempt (#25664). Contributed by @kerryarchibald.
* feat(faq): remove keyboard shortcuts button (#9342). Fixes #22625. Contributed by @gefgu.
* GYU: Update banner (#11211). Fixes #25530. Contributed by @justjanne.
* Linkify mxc:// URLs as links to your media repo (#11213). Fixes #6942.
* OIDC: Log in (#11199). Fixes #25657. Contributed by @kerryarchibald.
* Handle all permitted url schemes in linkify (#11215). Fixes #4457 and #8720.
* Autoapprove Element Call oidc requests (#11209). Contributed by @toger5.
* Allow creating knock rooms (#11182). Contributed by @charlynguyen.
* Expose and pre-populate thread ID in devtools dialog (#10953).
* Hide URL preview if it will be empty (#9029).
* Change wording from avatar to profile picture (#7015). Fixes vector-im/element-meta#1331. Contributed by @aaronraimist.
* Quick and dirty devtool to explore state history (#11197).
* Consider more user inputs when calculating zxcvbn score (#11180).
* GYU: Account Notification Settings (#11008). Fixes #24567. Contributed by @justjanne.
* Compound Typography pass (#11103). Fixes #25548.
* OIDC: navigate to authorization endpoint (#11096). Fixes #25574. Contributed by @kerryarchibald.
* Fix read receipt sending behaviour around thread roots (#3600).
* Fix missing metaspace notification badges (#11269). Fixes #25679.
* Make checkboxes less rounded (#11224). Contributed by @andybalaam.
* GYU: Fix issues with audible keywords without activated mentions (#11218). Contributed by @justjanne.
* PosthogAnalytics unwatch settings on logout (#11207). Fixes #25703.
* Avoid trying to set room account data for pinned events as guest (#11216). Fixes #6300.
* GYU: Disable sound for DMs checkbox when DM notifications are disabled (#11210). Contributed by @justjanne.
* force to allow calls without video and audio in embedded mode (#11131). Contributed by @EnricoSchw.
* Fix room tile text clipping (#11196). Fixes #25718.
* Handle newlines in user pills (#11166). Fixes #10994.
* Limit width of user menu in space panel (#11192). Fixes #22627.
* Add isLocation to ComposerEvent analytics events (#11187). Contributed by @andybalaam.
* Fix: hide unsupported login elements (#11185). Fixes #25711. Contributed by @kerryarchibald.
* Scope smaller font size to user info panel (#11178). Fixes #25683.
* Apply i18n to strings in the html export (#11176).
* Inhibit url previews on MXIDs containing slashes same as those without (#11160).
* Make event info size consistent with state events (#11181).
* Fix markdown content spacing (#11177). Fixes #25685.
* Fix font-family definition for emojis (#11170). Fixes #25686.
* Fix spurious error sending receipt in thread errors (#11157).
* Consider the empty push rule actions array equiv to deprecated dont_notify (#11155). Fixes #25674.
* Only trap escape key for cancel reply if there is a reply (#11140). Fixes #25640.
* Update linkify to 4.1.1 (#11132). Fixes #23806.

[1.8.37]
* Update Element to 1.11.37
* [Full changelog](https://github.com/vector-im/element-web/releases/tag/v1.11.37)
* Do not show "Forget room" button in Room View header for guest users (#10898). Contributed by @spantaleev.
* Switch to updating presence via /sync calls instead of PUT /presence (#11223). Fixes #20809 #13877 and #4813.
* Fix blockquote colour contrast (#11299). Fixes matrix-org/element-web-rageshakes#21800.
* Don't hide room header buttons in video rooms and rooms with a call (#9712). Fixes #23900.
* OIDC: Persist details in session storage, create store (#11302). Fixes #25710. Contributed by @kerryarchibald.
* Allow setting room join rule to knock (#11248). Contributed by @charlynguyen.
* Retry joins on 524 (Cloudflare timeout) also (#11296). Fixes #8776.
* Make sure users returned by the homeserver search API are displayed. Don't silently drop any. (#9556). Fixes #24422. Contributed by @maxmalek.
* Offer to unban user during invite if inviter has sufficient permissions (#11256). Fixes #3222.
* Split join and goto slash commands, the latter shouldn't auto_join (#11259). Fixes #10128.
* Integration work for rich text editor 2.3.1 (#11172). Contributed by @alunturner.
* Compound color pass (#11079). Fixes vector-im/element-internal#450 and #25547.
* Warn when demoting self via /op and /deop slash commands (#11214). Fixes #13726.

[1.8.38]
* Update Element to 1.11.38
* [Full changelog](https://github.com/vector-im/element-web/releases/tag/v1.11.38)
* Package release builds of element-web in package.element.io debs (#25198).
* Revert to using the /presence API for presence (#11366)

[1.8.39]
* Update Element to 1.11.39
* [Full changelog](https://github.com/vector-im/element-web/releases/tag/v1.11.39)
* Deprecate camelCase config options (#25800).
* Deprecate customisations in favour of Module API (#25736). Fixes #25733.
* Update labs.md for knock rooms (#25923). Contributed by @charlynguyen.
* Package release builds of element-web in package.element.io debs (#25198).
* Allow knocking rooms (#11353). Contributed by @charlynguyen.
* Support adding space-restricted joins on rooms not members of those spaces (#9017). Fixes #19213.
* Clear requiresClient and show pop-out if widget-api fails to ready (#11321). Fixes vector-im/customer-retainer#73.
* Bump pagination sizes due to hidden events (#11342).
* Remove display of key backup signatures from backup settings (#11333).
* Use PassphraseFields in ExportE2eKeysDialog to enforce minimum passphrase complexity (#11222). Fixes #9478.
* Fix "Export chat" not respecting configured time format in plain text mode (#10696). Fixes #23838. Contributed by @rashmitpankhania.
* Fix some missing 1-count pluralisations around event list summaries (#11371). Fixes #25925.
* Fix create subspace dialog not working for public space creation (#11367). Fixes #25916.
* Search for users on paste (#11304). Fixes #17523. Contributed by @peterscheu-aceart.
* Fix AppTile context menu not always showing up when it has options (#11358). Fixes #25914.
* Fix clicking on home all rooms space notification not working (#11337). Fixes #22844.
* Fix joining a suggested room switching space away (#11347). Fixes #25838.
* Fix home/all rooms context menu in space panel (#11350). Fixes #25896.
* Make keyboard handling in and out of autocomplete completions consistent (#11344). Fixes #25878.
* De-duplicate reactions by sender to account for faulty/malicious servers (#11340). Fixes #25872.
* Fix disable_3pid_login being ignored for the email field (#11335). Fixes #25863.
* Upgrade wysiwyg editor for ctrl+backspace windows fix (#11324). Fixes vector-im/verticals-internal#102.
* Unhide the view source event toggle - it works well enough (#11336). Fixes #25861.

[1.8.40]
* Update Element to 1.11.40
* [Full changelog](https://github.com/vector-im/element-web/releases/tag/v1.11.40)
* Hide account deactivation for externally managed accounts (#11445). Fixes #26022. Contributed by @kerryarchibald.
* OIDC: Redirect to delegated auth provider when signing out (#11432). Fixes #26000. Contributed by @kerryarchibald.
* Disable 3pid fields in settings when m.3pid_changes capability is disabled (#11430). Fixes #25995. Contributed by @kerryarchibald.
* OIDC: disable multi session signout for OIDC-aware servers in session manager (#11431). Contributed by @kerryarchibald.
* Implement updated open dialog method of the Module API (#11395). Contributed by @dhenneke.
* Polish & delabs Exploring public spaces feature (#11423).
* Treat lists with a single empty item as plain text, not Markdown. (#6833). Fixes vector-im/element-meta#1265.
* Allow managing room knocks (#11404). Contributed by @charlynguyen.
* Pin the action buttons to the bottom of the scrollable dialogs (#11407). Contributed by @dhenneke.
* Support Matrix 1.1 (drop legacy r0 versions) (#9819).
* Fix path separator for Windows based systems (#25997).
* Fix instances of double translation and guard translation calls using typescript (#11443).
* Fix export type "Current timeline" to match its behaviour to its name (#11426). Fixes #25988.
* Fix Room Settings > Notifications file upload input being shown superfluously (#11415). Fixes #18392.
* Simplify registration with email validation (#11398). Fixes #25832 #23601 and #22297.
* correct home server URL (#11391). Fixes #25931. Contributed by @NSV1991.
* Include non-matching DMs in Spotlight recent conversations when the DM's userId is part of the search API results (#11374). Contributed by @mgcm.
* Fix useRoomMembers missing updates causing incorrect membership counts (#11392). Fixes #17096.
* Show error when searching public rooms fails (#11378).

[1.9.0]
* Update Element to 1.11.41
* [Full changelog](https://github.com/vector-im/element-web/releases/tag/v1.11.41)
* Make SVGR icons use forward ref (#26082).
* Add support for rendering a custom wrapper around Element (#25537). Contributed by @maheichyk.
* Allow creating public knock rooms (#11481). Contributed by @charlynguyen.
* Render custom images in reactions according to MSC4027 (#11087). Contributed by @sumnerevans.
* Introduce room knocks bar (#11475). Contributed by @charlynguyen.
* Room header UI updates (#11507). Fixes #25892.

[1.9.1]
* Update Element to 1.11.42
* [Full changelog](https://github.com/vector-im/element-web/releases/tag/v1.11.42)
* Update Compound to fix Firefox-specific avatar regression (#11604). Fixes #26155.

[1.9.2]
* Update Element to 1.11.43
* [Full changelog](https://github.com/vector-im/element-web/releases/tag/v1.11.43)

[1.9.3]
* Update Element to 1.11.44
* [Full changelog](https://github.com/vector-im/element-web/releases/tag/v1.11.44)
* Make video & voice call buttons pin conference widget if unpinned (#11576). Fixes vector-im/customer-retainer#72.
* OIDC: persist refresh token (#11249). Contributed by @kerryarchibald.
* ElementR: Cross user verification (#11364). Fixes #25752. Contributed by @florianduros.
* Default intentional mentions (#11602).
* Notify users about denied access on ask-to-join rooms (#11480). Contributed by @nurjinjafar.
* Allow setting knock room directory visibility (#11529). Contributed by @charlynguyen.

[1.9.4]
* Update Element to 1.11.45
* [Full changelog](https://github.com/vector-im/element-web/releases/tag/v1.11.45)
* Fix Emoji font on Safari 17 (#11673).

[1.9.5]
* Update Element to 1.11.46
* [Full changelog](https://github.com/vector-im/element-web/releases/tag/v1.11.46)
* Use .well-known to discover a default rendezvous server for use with Sign in with QR (#11655). Contributed by @hughns.
* Message layout will update according to the selected style (#10170). Fixes #21782. Contributed by @manancodes.
* Implement MSC4039: Add an MSC for a new Widget API action to upload files into the media repository (#11311). Contributed by @dhenneke.
* Render space pills with square corners to match new avatar (#11632). Fixes #26056.

[1.9.6]
* Update Element to 1.11.47
* [Full changelog](https://github.com/vector-im/element-web/releases/tag/v1.11.47)
* Iterate `io.element.late_event` decoration (#11760). Fixes #26384.
* Render timeline separator for late event groups (#11739).
* OIDC: revoke tokens on logout (#11718). Fixes #25394. Contributed by @kerryarchibald.
* Show `io.element.late_event` in MessageTimestamp when known (#11733).
* Show all labs flags if developerMode enabled (#11746). Fixes #24571 and #8498.
* Use Compound tooltips on MessageTimestamp to improve UX of date time discovery (#11732). Fixes #25913.
* Consolidate 4s passphrase input fields and use stable IDs (#11743). Fixes #26228.
* Disable upgraderoom command without developer mode enabled (#11744). Fixes #17620.

[1.9.7]
* Update Element to 1.11.48
* [Full changelog](https://github.com/vector-im/element-web/releases/tag/v1.11.48)
* Correctly fill window.matrixChat even when a Wrapper module is active (#26395). Contributed by @dhenneke.
* Knock on a ask-to-join room if a module wants to join the room when navigating to a room (#11787). Contributed by @dhenneke.
* Element-R: Include crypto info in sentry (#11798). Contributed by @florianduros.
* Element-R: Include crypto info in rageshake (#11797). Contributed by @florianduros.
* Element-R: Add current version of the rust-sdk and vodozemac (#11785). Contributed by @florianduros.

[1.9.8]
* Update Element to 1.11.49
* [Full changelog](https://github.com/vector-im/element-web/releases/tag/v1.11.49)
* Ensure setUserCreator is called when a store is assigned (#3867). Fixes #26520

[1.9.9]
* Update Element to 1.11.50
* [Full changelog](https://github.com/vector-im/element-web/releases/tag/v1.11.50)
* Ship element-web as a debian package (#26533). Contributed by @t3chguy.
* Update room summary card header (#11823). Contributed by @germain-gg.
* Add feature flag for disabling encryption in Element Call (#11837). Contributed by @toger5.
* Adapt the rendering of extra icons in the room header (#11835). Contributed by @charlynguyen.
* Implement new unreachable state and fix broken string ref (#11748). Contributed by @MidhunSureshR.
* Allow adding extra icons to the room header (#11799). Contributed by @charlynguyen.
* Room header: do not collapse avatar or facepile (#11866). Contributed by @kerryarchibald.
* New right panel: fix button alignment in memberlist (#11861). Contributed by @kerryarchibald.
* Use the correct video call icon variant (#11859). Contributed by @robintown.
* fix broken warning icon (#11862). Contributed by @ara4n.
* Fix rightpanel hiding scrollbar (#11831). Contributed by @kerryarchibald.
* Switch to updating presence via /sync calls instead of PUT /presence (#11824). Contributed by @t3chguy.

[1.9.10]
* Update Element to 1.11.51
* [Full changelog](https://github.com/vector-im/element-web/releases/tag/v1.11.51)
* Improve debian package and docs (#26618). Contributed by @t3chguy.
* Remove Quote from MessageContextMenu as it is unsupported by WYSIWYG (#11914). Contributed by @t3chguy.
* Always allow call.member events on new rooms (#11948). Contributed by @toger5.
* Right panel: view third party invite info without clearing history (#11934). Contributed by @kerryarchibald.
* Allow switching to system emoji font (#11925). Contributed by @t3chguy.
* Update open in other tab message (#11916). Contributed by @weeman1337.
* Add menu for legacy and element call in 1:1 rooms (#11910). Contributed by @toger5.
* Add ringing for matrixRTC (#11870). Contributed by @toger5.
* Keep device language when it has been previosuly set, after a successful delegated authentication flow that clears localStorage (#11902). Contributed by @mgcm.
* Fix misunderstanding of functional members (#11918). Contributed by @toger5.
* Fix: Video Room Chat Header Button Removed (#11911). Contributed by @kerryarchibald.
* Fix "not attempting encryption" warning (#11899). Contributed by @richvdh.

[1.9.11]
* Update Element to 1.11.52
* [Full changelog](https://github.com/vector-im/element-web/releases/tag/v1.11.52)
* Keep more recent rageshake logs (#12003). Contributed by @richvdh.
* Fix bug which prevented correct clean up of rageshake store (#12002). Contributed by @richvdh.
* Set up key backup using non-deprecated APIs (#12005). Contributed by @andybalaam.
* Fix notifications appearing for old events (#3946). Contributed by @dbkr.
* Prevent phantom notifications from events not in a room's timeline (#3942). Contributed by @dbkr.

[1.9.12]
* Update Element to 1.11.53
* [Full changelog](https://github.com/vector-im/element-web/releases/tag/v1.11.53)
* Fix a fresh login creating a new key backup (#12106).

[1.9.13]
* Update Element to 1.11.54
* [Full changelog](https://github.com/element-hq/element-web/releases/tag/v1.11.54)
* Fix CSS stacking context order determinism (#26840). Contributed by @t3chguy.
* Accessibility improvements around aria-labels and tooltips (#12062). Contributed by @t3chguy.
* Add RoomKnocksBar to RoomHeader (#12077). Contributed by @charlynguyen.
* Adjust tooltip side for DecoratedRoomAvatar to not obscure room name (#12079). Contributed by @t3chguy.
* Iterate landmarks around the app in order to improve a11y (#12064). Contributed by @t3chguy.
* Update element call embedding UI (#12056). Contributed by @toger5.
* Use Compound tooltips instead of homegrown in TextWithTooltip & InfoTooltip (#12052). Contributed by @t3chguy.
* Fix regression around CSS stacking contexts and PIP widgets (#12094). Contributed by @t3chguy.
* Fix Identity Server terms accepting not working as expected (#12109). Contributed by @t3chguy.
* fix: microphone and camera dropdown doesn't work In legacy call (#12105). Contributed by @muratersin.
* Revert "Set up key backup using non-deprecated APIs (#12005)" (#12102). Contributed by @BillCarsonFr.
* Fix regression around read receipt animation from refs changes (#12100). Contributed by @t3chguy.
* Added meaning full error message based on platform (#12074). Contributed by @Pankaj-SinghR.
* Fix editing event from search room view (#11992). Contributed by @t3chguy.
* Fix timeline position when moving to a room and coming back (#12055). Contributed by @florianduros.
* Fix threaded reply playwright tests (#12070). Contributed by @dbkr.
* Element-R: fix repeated requests to enter 4S key during cross-signing reset (#12059). Contributed by @richvdh.
* Fix position of thumbnail in room timeline (#12016). Contributed by @anoopw3bdev.

[1.9.14]
* Update Element to 1.11.55
* [Full changelog](https://github.com/element-hq/element-web/releases/tag/v1.11.55)
* Broaden support for matrix spec versions (#12159). Contributed by @RiotRobot.
* Fixed shield alignment on message Input (#12155). Contributed by @RiotRobot.

[1.9.15]
* Update Element to 1.11.57
* [Full changelog](https://github.com/element-hq/element-web/releases/tag/v1.11.57)
* Expose apps/widgets (#12071). Contributed by @charlynguyen.
* Enable the rust-crypto labs button (#12114). Contributed by @richvdh.
* Show a progress bar while migrating from legacy crypto (#12104). Contributed by @richvdh.
* Update Twemoji to Jdecked v15.0.3 (#12147). Contributed by @t3chguy.
* Change Quick Settings icon (#12141). Contributed by @florianduros.
* Use Compound tooltips more widely (#12128). Contributed by @t3chguy.
* Fix OIDC bugs due to amnesiac stores forgetting OIDC issuer & other data (#12166). Contributed by @t3chguy.
* Fix account management link for delegated auth OIDC setups (#12144). Contributed by @t3chguy.
* Fix Safari IME support (#11016). Contributed by @SuperKenVery.
* Fix Stickerpicker layout crossing multiple CSS stacking contexts (#12127).
* Fix Stickerpicker layout crossing multiple CSS stacking contexts (#12126). Contributed by @t3chguy.
* Fix 1F97A and 1F979 in Twemoji COLR font (#12177).

[1.9.16]
* Update Element to 1.11.58
* [Full changelog](https://github.com/element-hq/element-web/releases/tag/v1.11.58)
* The flag to enable the Rust crypto implementation is now set to true by default. This means that without any additional configuration every new login will use the new cryptography implementation.
* Add Element call related functionality to new room header (#12091). Contributed by @toger5.
* Add labs flag for Threads Activity Centre (#12137). Contributed by @florianduros.
* Refactor element call lobby + skip lobby (#12057). Contributed by @toger5.
* Hide the "Message" button in the sidebar if the CreateRooms components should not be shown (#9271). Contributed by @dhenneke.
* Add notification dots to thread summary icons (#12146). Contributed by @dbkr.
* [Backport staging] Fix the StorageManger detecting a false positive consistency check when manually migrating to rust from labs (#12230). Contributed by @RiotRobot.
* Fix logout can take ages (#12191). Contributed by @BillCarsonFr.
* Fix Mark all as read in settings (#12205). Contributed by @florianduros.
* Fix default thread notification of the new RoomHeader (#12194). Contributed by @florianduros.
* Fix display of room notification debug info (#12183). Contributed by @dbkr.

[1.9.17]
* Update Element to 1.11.59
* [Full changelog](https://github.com/element-hq/element-web/releases/tag/v1.11.59)
* Ignore activity in TAC (#12269). Contributed by @florianduros.
* Use browser's font size instead of hardcoded 16px as root font size (#12246). Contributed by @florianduros.
* Revert "Use Compound primary colors for most actions" (#12264). Contributed by @florianduros.
* Revert "Refine menu, toast, and popover colors" (#12263). Contributed by @florianduros.
* Fix Native OIDC for Element Desktop (#12253). Contributed by @t3chguy.
* Improve client metadata used for OIDC dynamic registration (#12257). Contributed by @t3chguy.
* Refine menu, toast, and popover colors (#12247). Contributed by @robintown.
* Call the AsJson forms of import and exportRoomKeys (#12233). Contributed by @andybalaam.
* Use Compound primary colors for most actions (#12241). Contributed by @robintown.
* Enable redirected media by default (#12142). Contributed by @turt2live.
* Reduce TAC width by 16px (#12239). Contributed by @florianduros.
* Pop out of Threads Activity Centre (#12136). Contributed by @florianduros.
* Use new semantic tokens for username colors (#12209). Contributed by @robintown.

[1.9.18]
* Update Element to 1.11.60
* [Full changelog](https://github.com/element-hq/element-web/releases/tag/v1.11.60)
* Refine styles of controls to match Compound (#12299). Contributed by @robintown.
* Hide the archived section (#12286). Contributed by @dbkr.
* Add theme data to EC widget Url (#12279). Contributed by @toger5.
* Update MSC2965 OIDC Discovery implementation (#12245). Contributed by @t3chguy.
* Use green dot for activity notification in LegacyRoomHeader (#12270). Contributed by @florianduros.

[1.9.19]
* Update Element to 1.11.61
* [Full changelog](https://github.com/element-hq/element-web/releases/tag/v1.11.61)
* Update @vector-im/compound-design-tokens in package.json (#12340).

[1.9.20]
* Update Element to 1.11.62
* [Full changelog](https://github.com/element-hq/element-web/releases/tag/v1.11.62)
* Change user permission by using a new apply button (#12346). Contributed by @florianduros.
* Mark as Unread (#12254). Contributed by @dbkr.
* Refine the colors of some more components (#12343). Contributed by @robintown.
* TAC: Order rooms by most recent after notification level (#12329). Contributed by @florianduros.
* Make EC widget theme reactive - Update widget url when the theme changes (#12295). Contributed by @toger5.
* Refine styles of menus, toasts, popovers, and modals (#12332). Contributed by @robintown.
* Element Call: fix widget shown while its still loading (waitForIframeLoad=false) (#12292). Contributed by @toger5.
* Improve Forward Dialog a11y by switching to roving tab index interactions (#12306). Contributed by @t3chguy.
* Call guest access link creation to join calls as a non registered user via the EC SPA (#12259). Contributed by @toger5.
* Use strong element to semantically denote visually emphasised content (#12320). Contributed by @t3chguy.
* Handle up/down arrow keys as well as left/right for horizontal toolbars for improved a11y (#12305). Contributed by @t3chguy.
* [Backport staging] Remove the glass border from modal spinners (#12369). Contributed by @RiotRobot.
* Fix incorrect check for private read receipt support (#12348). Contributed by @tulir.
* TAC: Fix hover state when expanded (#12337). Contributed by @florianduros.
* Fix the image view (#12341). Contributed by @robintown.
* Use correct push rule to evaluate room-wide mentions (#12318). Contributed by @t3chguy.
* Reset power selector on API failure to prevent state mismatch (#12319). Contributed by @t3chguy.
* Fix spotlight opening in TAC (#12315). Contributed by @florianduros.

[1.9.21]
* Update Element to 1.11.63
* [Full changelog](https://github.com/element-hq/element-web/releases/tag/v1.11.63)
* Revert "Make EC widget theme reactive - Update widget url when the theme changes" (#12383) in order to fix widgets that require authentication.
* Update to hotfixed js-sdk to fix an issue where Element could try to set a push rule in a loop.

[1.9.22]
* Update Element to 1.11.64
* [Full changelog](https://github.com/element-hq/element-web/releases/tag/v1.11.64)
* Mark all threads as read button (#12378). Contributed by @dbkr.
* Video call meta space (#12297). Contributed by @toger5.
* Add leave room warning for last admin (#9452). Contributed by @Arnei.
* Iterate styles around Link new device via QR (#12356). Contributed by @t3chguy.
* Improve code-splitting of highlight.js and maplibre-gs libs (#12349). Contributed by @t3chguy.
* Use data-mx-color for rainbows (#12325). Contributed by @tulir.
* Fix external guest access url for unencrypted rooms (#12345). Contributed by @toger5.
* Fix video rooms not showing share link button (#12374). Contributed by @toger5.
* Fix space topic jumping on hover/focus (#12377). Contributed by @t3chguy.
* Allow popping out a Jitsi widget to respect Desktop web_base_url config (#12376). Contributed by @t3chguy.
* Remove the Lazy Loading InvalidStoreError Dialogs (#12358). Contributed by @langleyd.
* Improve readability of badges and pills (#12360). Contributed by @robintown.

[1.9.23]
* Update Element to 1.11.65
* [Full changelog](https://github.com/element-hq/element-web/releases/tag/v1.11.65)
* Make empty state copy for TAC depend on the value of the setting (#12419). Contributed by @dbkr.
* Linkify User Interactive Authentication errors (#12271). Contributed by @t3chguy.
* Add support for device dehydration v2 (#12316). Contributed by @uhoreg.
* Replace SecurityCustomisations with CryptoSetupExtension (#12342). Contributed by @thoraj.
* Add activity toggle for TAC (#12413). Contributed by @dbkr.
* Humanize spell check language labels (#12409). Contributed by @t3chguy.
* Call Guest Access, give user the option to change the acces level so they can generate a call link. (#12401). Contributed by @toger5.
* TAC: Release Announcement (#12380). Contributed by @florianduros.
* Show the call and share button if the user can create a guest link. (#12385). Contributed by @toger5.
* Add analytics for mark all threads unread (#12384). Contributed by @dbkr.
* Add EventType.RoomEncryption to the auto approve capabilities of Element Call widgets (#12386). Contributed by @toger5.
* Fix link modal not shown after access upgrade (#12411). Contributed by @toger5.
* Fix thread navigation in timeline (#12412). Contributed by @florianduros.
* Fix inability to join a knock room via space hierarchy view (#12404). Contributed by @t3chguy.
* Focus the thread panel when clicking on an item in the TAC (#12410). Contributed by @dbkr.
* Fix space hierarchy tile busy state being stuck after join error (#12405). Contributed by @t3chguy.
* Fix room topic in-app links not being handled correctly on topic dialog (#12406). Contributed by @t3chguy.

[1.9.24]
* Update Element to 1.11.66
* [Full changelog](https://github.com/element-hq/element-web/releases/tag/v1.11.66)
* Use a different error message for UTDs when you weren't in the room. (#12453). Contributed by @uhoreg.
* Take the Threads Activity Centre out of labs (#12439). Contributed by @dbkr.
* Expected UTDs: use a different message for UTDs sent before login (#12391). Contributed by @richvdh.
* Add Tooltip to AccessibleButton (#12443). Contributed by @florianduros.
* Add analytics to activity toggles (#12418). Contributed by @dbkr.
* Decrypt events in reverse order without copying the array (#12445). Contributed by @Johennes.
* Use new compound tooltip (#12416). Contributed by @florianduros.
* Expected UTDs: report a different Posthog code (#12389). Contributed by @richvdh.
* TAC: Fix accessibility issue when the Release announcement is displayed (#12484). Contributed by @RiotRobot.
* TAC: Close Release Announcement when TAC button is clicked (#12485). Contributed by @florianduros.
* MenuItem: fix caption usage (#12455). Contributed by @florianduros.
* Show the local echo in previews (#12451). Contributed by @langleyd.
* Fixed the drag and drop of X #27186 (#12450). Contributed by @asimdelvi.
* Move the TAC to above the button (#12438). Contributed by @dbkr.
* Use the same logic in previews as the timeline to hide events that should be hidden (#12434). Contributed by @langleyd.
* Fix selector so maths support doesn't mangle divs (#12433). Contributed by @uhoreg.

[1.9.25]
* Update Element to 1.11.67
* [Full changelog](https://github.com/element-hq/element-web/releases/tag/v1.11.67)
* Tooltip: Improve the accessibility of the composer and the rich text editor (#12459). Contributed by @florianduros.
* Allow explicit configuration of OIDC dynamic registration metadata (#12514). Contributed by @t3chguy.
* Tooltip: improve accessibility for messages (#12487). Contributed by @florianduros.
* Collapse UserSettings tabs to just icons on narrow screens (#12505). Contributed by @dbkr.
* Add room topic to right panel room info (#12503). Contributed by @t3chguy.
* OIDC: pass id_token via id_token_hint on Manage Account interaction (#12499). Contributed by @t3chguy.
* Tooltip: improve accessibility in room (#12493). Contributed by @florianduros.
* Tooltip: improve accessibility for call and voice messages (#12489). Contributed by @florianduros.
* Move the active tab in user settings to the dialog title (#12481). Contributed by @dbkr.
* Tooltip: improve accessibility of spaces (#12497). Contributed by @florianduros.
* Tooltip: improve accessibility of the right panel (#12490). Contributed by @florianduros.
* MSC3575 (Sliding Sync) add well-known proxy support (#12307). Contributed by @EdGeraghty.
* Reuse single PlaybackWorker between Playback instances (#12520). Contributed by @t3chguy.
* Fix well-known lookup for sliding sync labs check (#12519). Contributed by @t3chguy.
* Fix element-desktop-ssoid being included in OIDC Authorization call (#12495). Contributed by @t3chguy.
* Fix beta notifications reconciliation for intentional mentions push rules (#12510). Contributed by @t3chguy.
* fix avatar stretched on 1:1 call (#12494). Contributed by @I-lander.
* Check native sliding sync support against an unstable feature flag (#12498). Contributed by @turt2live.
* Use OPTIONS for sliding sync detection poke (#12492). Contributed by @turt2live.
* TAC: hide tooltip when the release announcement is displayed (#12472). Contributed by @florianduros.

[1.9.26]
* Update Element to 1.11.68
* [Full changelog](https://github.com/element-hq/element-web/releases/tag/v1.11.68)
* Tooltip: Improve accessibility for context menus (#12462). Contributed by @florianduros.
* Tooltip: Improve accessibility of space panel (#12525). Contributed by @florianduros.

[1.9.27]
* Update Element to 1.11.69
* [Full changelog](https://github.com/element-hq/element-web/releases/tag/v1.11.69)

[1.9.28]
* Update Element to 1.11.70
* [Full changelog](https://github.com/element-hq/element-web/releases/tag/v1.11.70)

[1.9.29]
* Update Element to 1.11.71
* [Full changelog](https://github.com/element-hq/element-web/releases/tag/v1.11.71)

[1.9.30]
* Update Element to 1.11.72
* [Full changelog](https://github.com/element-hq/element-web/releases/tag/v1.11.72)
* Polyfill Intl.Segmenter for wider web browser compatibility (#27803). Contributed by @dbkr.
* Enable audio/webaudio Modernizr rule (#27772). Contributed by @t3chguy.
* Add release announcement for the new room header (#12802). Contributed by @MidhunSureshR.
* Default the room header to on (#12803). Contributed by @MidhunSureshR.
* Update Thread Panel to match latest designs (#12797). Contributed by @t3chguy.
* Close any open modals on logout (#12777). Contributed by @dbkr.
* Iterate design of right panel empty state (#12796). Contributed by @t3chguy.
* Update styling of UserInfo right panel card (#12788). Contributed by @t3chguy.
* Accessibility: Add Landmark navigation (#12190). Contributed by @akirk.
* Let Element Call widget receive m.room.create (#12710). Contributed by @AndrewFerr.
* Let Element Call widget set session memberships (#12713). Contributed by @AndrewFerr.
* Update right panel base card styling to match Compound (#12768). Contributed by @t3chguy.
* Align `widget_build_url_ignore_dm` with call behaviour switch between 1:1 and Widget (#12760). Contributed by @t3chguy.
* Move integrations switch (#12733). Contributed by @dbkr.
* Element-R: Report events with withheld keys separately to Posthog. (#12755). Contributed by @richvdh.

[1.9.31]
* Update Element to 1.11.73
* [Full changelog](https://github.com/element-hq/element-web/releases/tag/v1.11.73)
* Fixes for CVE-2024-42347 / GHSA-f83w-wqhc-cfp4

[1.9.32]
* Update Element to 1.11.74
* [Full changelog](https://github.com/element-hq/element-web/releases/tag/v1.11.74)

[1.9.33]
* Update Element to 1.11.75
* [Full changelog](https://github.com/element-hq/element-web/releases/tag/v1.11.75)

[1.9.34]
* Update Element to 1.11.76
* [Full changelog](https://github.com/element-hq/element-web/releases/tag/v1.11.76)

[1.9.35]
* Update Element to 1.11.77
* [Full changelog](https://github.com/element-hq/element-web/releases/tag/v1.11.77)

[1.9.36]
* Update Element to 1.11.78
* [Full changelog](https://github.com/element-hq/element-web/releases/tag/v1.11.78)
* Add Release announcement for the pinning message list (#46). Contributed by @florianduros.
* Unlabs feature pinning (#22). Contributed by @florianduros.
* Add mobile registration (#42). Contributed by @langleyd.
* Add support for org.matrix.cross_signing_reset UIA stage flow (#34). Contributed by @t3chguy.
* Add timezone to user profile (#20). Contributed by @Half-Shot.
* Add config option to force verification (#29). Contributed by @dbkr.
* Reduce pinned message banner size (#28). Contributed by @florianduros.
* Enable message pinning labs by default (#25). Contributed by @florianduros.
* Remove release announcement of the new header (#23). Contributed by @florianduros.

[1.9.37]
* Update Element to 1.11.79
* [Full changelog](https://github.com/element-hq/element-web/releases/tag/v1.11.79)

[1.9.38]
* Update Element to 1.11.80
* [Full changelog](https://github.com/element-hq/element-web/releases/tag/v1.11.80)
* Add doc for 'force_verification config option (#​28035). Contributed by @​dbkr.
* Roll back change to device isolation mode (#​104). Contributed by @​richvdh.
* Remove right panel toggling behaviour on room header buttons (#​100). Contributed by @​t3chguy.
* Improve error display for messages sent from insecure devices (#​93). Contributed by @​richvdh.
* Add labs option to exclude unverified devices (#​92). Contributed by @​richvdh.
* Improve contrast for timestamps, date separators & spotlight trigger (#​91). Contributed by @​t3chguy.
* Open room settings on room header avatar click (#​88). Contributed by @​t3chguy.
* Use strong over b for improved a11y semantics (#​41). Contributed by @​t3chguy.
* Grant Element Call widget capabilities for "raise hand" feature (#​82). Contributed by @​AndrewFerr.
* Mobile registration optimizations and tests (#​62). Contributed by @​langleyd.
* Ignore chat effect when older than 48h (#​48). Contributed by @​florianduros.

[1.9.39]
* Update Element to 1.11.81
* [Full changelog](https://github.com/element-hq/element-web/releases/tag/v1.11.81)
* This release fixes High severity vulnerability CVE-2024-47771

[1.9.40]
* Update Element to 1.11.82
* [Full changelog](https://github.com/element-hq/element-web/releases/tag/v1.11.82)

[1.9.41]
* Update Element to 1.11.83
* [Full changelog](https://github.com/element-hq/element-web/releases/tag/v1.11.83)
* Enable Element Call by default on release instances

[1.9.42]
* Update Element to 1.11.84
* [Full Changelog](https://github.com/element-hq/element-web/releases/tag/v1.11.84)
* Remove abandoned MSC3886, MSC3903, MSC3906 implementations ([#&#8203;28274](https://github.com/element-hq/element-web/pull/28274)). Contributed by [@&#8203;t3chguy](https://github.com/t3chguy).
* Update to React 18 ([#&#8203;24763](https://github.com/element-hq/element-web/pull/24763)). Contributed by [@&#8203;t3chguy](https://github.com/t3chguy).
* Deduplicate icons using Compound ([#&#8203;28239](https://github.com/element-hq/element-web/pull/28239)). Contributed by [@&#8203;t3chguy](https://github.com/t3chguy).
* Replace legacy Tooltips with Compound tooltips ([#&#8203;28231](https://github.com/element-hq/element-web/pull/28231)). Contributed by [@&#8203;t3chguy](https://github.com/t3chguy).
* Deduplicate icons using Compound Design Tokens ([#&#8203;28219](https://github.com/element-hq/element-web/pull/28219)). Contributed by [@&#8203;t3chguy](https://github.com/t3chguy).
* Add reactions to html export ([#&#8203;28210](https://github.com/element-hq/element-web/pull/28210)). Contributed by [@&#8203;langleyd](https://github.com/langleyd).
* Remove feature_dehydration ([#&#8203;28173](https://github.com/element-hq/element-web/pull/28173)). Contributed by [@&#8203;florianduros](https://github.com/florianduros).
* Remove upgrade encryption in `DeviceListener` and `SetupEncryptionToast` ([#&#8203;28299](https://github.com/element-hq/element-web/pull/28299)). Contributed by [@&#8203;florianduros](https://github.com/florianduros).
* Fix 'remove alias' button in room settings ([#&#8203;28269](https://github.com/element-hq/element-web/pull/28269)). Contributed by [@&#8203;Dev-Gurjar](https://github.com/Dev-Gurjar).
* Add back unencrypted path in `StopGapWidgetDriver.sendToDevice` ([#&#8203;28295](https://github.com/element-hq/element-web/pull/28295)). Contributed by [@&#8203;florianduros](https://github.com/florianduros).
* Fix other devices not being decorated as such ([#&#8203;28279](https://github.com/element-hq/element-web/pull/28279)). Contributed by [@&#8203;t3chguy](https://github.com/t3chguy).
* Fix pill contrast in invitation dialog ([#&#8203;28250](https://github.com/element-hq/element-web/pull/28250)). Contributed by [@&#8203;florianduros](https://github.com/florianduros).
* Close right panel chat when minimising maximised voip widget ([#&#8203;28241](https://github.com/element-hq/element-web/pull/28241)). Contributed by [@&#8203;t3chguy](https://github.com/t3chguy).
* Fix develop changelog parsing ([#&#8203;28232](https://github.com/element-hq/element-web/pull/28232)). Contributed by [@&#8203;t3chguy](https://github.com/t3chguy).
* Fix Ctrl+F shortcut not working with minimised room summary card ([#&#8203;28223](https://github.com/element-hq/element-web/pull/28223)). Contributed by [@&#8203;t3chguy](https://github.com/t3chguy).
* Fix network dropdown missing checkbox & aria-checked ([#&#8203;28220](https://github.com/element-hq/element-web/pull/28220)). Contributed by [@&#8203;t3chguy](https://github.com/t3chguy).

[1.9.43]
* Update element-web to 1.11.85
* [Full Changelog](https://github.com/element-hq/element-web/releases/tag/v1.11.85)
* Fixes for CVE-2024-51750 / GHSA-w36j-v56h-q9pc
* Fixes for CVE-2024-51749 / GHSA-5486-384g-mcx2
* Update JS SDK with the fixes for CVE-2024-50336 / GHSA-xvg8-m4x3-w6xr

[1.9.44]
* Update element-web to 1.11.86
* [Full Changelog](https://github.com/element-hq/element-web/releases/tag/v1.11.86)
* Deduplicate icons using Compound Design Tokens ([#&#8203;28419](https://github.com/element-hq/element-web/pull/28419)). Contributed by [@&#8203;t3chguy](https://github.com/t3chguy).
* Let widget driver send error details ([#&#8203;28357](https://github.com/element-hq/element-web/pull/28357)). Contributed by [@&#8203;AndrewFerr](https://github.com/AndrewFerr).
* Deduplicate icons using Compound Design Tokens ([#&#8203;28381](https://github.com/element-hq/element-web/pull/28381)). Contributed by [@&#8203;t3chguy](https://github.com/t3chguy).
* Auto approvoce `io.element.call.reaction` capability for element call widgets ([#&#8203;28401](https://github.com/element-hq/element-web/pull/28401)). Contributed by [@&#8203;toger5](https://github.com/toger5).
* Show message type prefix in thread root & reply previews ([#&#8203;28361](https://github.com/element-hq/element-web/pull/28361)). Contributed by [@&#8203;t3chguy](https://github.com/t3chguy).
* Support sending encrypted to device messages from widgets ([#&#8203;28315](https://github.com/element-hq/element-web/pull/28315)). Contributed by [@&#8203;hughns](https://github.com/hughns).

[1.9.45]
* Update element-web to 1.11.87
* [Full Changelog](https://github.com/element-hq/element-web/releases/tag/v1.11.87)
* Send and respect MSC4230 is_animated flag ([#&#8203;28513](https://github.com/element-hq/element-web/pull/28513)). Contributed by [@&#8203;t3chguy](https://github.com/t3chguy).
* Display a warning when an unverified user's identity changes ([#&#8203;28211](https://github.com/element-hq/element-web/pull/28211)). Contributed by [@&#8203;uhoreg](https://github.com/uhoreg).
* Swap out Twitter link for Mastodon on auth footer ([#&#8203;28508](https://github.com/element-hq/element-web/pull/28508)). Contributed by [@&#8203;t3chguy](https://github.com/t3chguy).
* Consider `org.matrix.msc3417.call` as video room in create room dialog ([#&#8203;28497](https://github.com/element-hq/element-web/pull/28497)). Contributed by [@&#8203;t3chguy](https://github.com/t3chguy).
* Standardise icons using Compound Design Tokens ([#&#8203;28217](https://github.com/element-hq/element-web/pull/28217)). Contributed by [@&#8203;t3chguy](https://github.com/t3chguy).
* Start sending stable `m.marked_unread` events ([#&#8203;28478](https://github.com/element-hq/element-web/pull/28478)). Contributed by [@&#8203;tulir](https://github.com/tulir).
* Upgrade to compound-design-tokens v2 ([#&#8203;28471](https://github.com/element-hq/element-web/pull/28471)). Contributed by [@&#8203;t3chguy](https://github.com/t3chguy).
* Standardise icons using Compound Design Tokens ([#&#8203;28286](https://github.com/element-hq/element-web/pull/28286)). Contributed by [@&#8203;t3chguy](https://github.com/t3chguy).
* Remove reply fallbacks as per merged MSC2781 ([#&#8203;28406](https://github.com/element-hq/element-web/pull/28406)). Contributed by [@&#8203;t3chguy](https://github.com/t3chguy).
* Use React Suspense when rendering async modals ([#&#8203;28386](https://github.com/element-hq/element-web/pull/28386)). Contributed by [@&#8203;t3chguy](https://github.com/t3chguy).
* Add spinner when room encryption is loading in room settings ([#&#8203;28535](https://github.com/element-hq/element-web/pull/28535)). Contributed by [@&#8203;florianduros](https://github.com/florianduros).
* Fix getOidcCallbackUrl for Element Desktop ([#&#8203;28521](https://github.com/element-hq/element-web/pull/28521)). Contributed by [@&#8203;t3chguy](https://github.com/t3chguy).
* Filter out redacted poll votes to avoid crashing the Poll widget ([#&#8203;28498](https://github.com/element-hq/element-web/pull/28498)). Contributed by [@&#8203;t3chguy](https://github.com/t3chguy).
* Fix force tab complete not working since switching to React 18 createRoot API ([#&#8203;28505](https://github.com/element-hq/element-web/pull/28505)). Contributed by [@&#8203;t3chguy](https://github.com/t3chguy).
* Fix media captions in bubble layout ([#&#8203;28480](https://github.com/element-hq/element-web/pull/28480)). Contributed by [@&#8203;tulir](https://github.com/tulir).
* Reset cross-signing before backup when resetting both ([#&#8203;28402](https://github.com/element-hq/element-web/pull/28402)). Contributed by [@&#8203;uhoreg](https://github.com/uhoreg).
* Listen to events so that encryption icon updates when status changes ([#&#8203;28407](https://github.com/element-hq/element-web/pull/28407)). Contributed by [@&#8203;uhoreg](https://github.com/uhoreg).
* Check that the file the user chose has a MIME type of `image/*` ([#&#8203;28467](https://github.com/element-hq/element-web/pull/28467)). Contributed by [@&#8203;t3chguy](https://github.com/t3chguy).
* Fix download button size in message action bar ([#&#8203;28472](https://github.com/element-hq/element-web/pull/28472)). Contributed by [@&#8203;t3chguy](https://github.com/t3chguy).
* Allow tab completing users in brackets ([#&#8203;28460](https://github.com/element-hq/element-web/pull/28460)). Contributed by [@&#8203;t3chguy](https://github.com/t3chguy).
* Fix React 18 strict mode breaking spotlight dialog ([#&#8203;28452](https://github.com/element-hq/element-web/pull/28452)). Contributed by [@&#8203;MidhunSureshR](https://github.com/MidhunSureshR).

[1.9.46]
* Update element-web to 1.11.88
* [Full Changelog](https://github.com/element-hq/element-web/releases/tag/v1.11.88)
* Allow trusted Element Call widget to send and receive media encryption key to-device messages ([#&#8203;28316](https://github.com/element-hq/element-web/pull/28316)). Contributed by [@&#8203;hughns](https://github.com/hughns).
* increase ringing timeout from 10 seconds to 90 seconds ([#&#8203;28630](https://github.com/element-hq/element-web/pull/28630)). Contributed by [@&#8203;fkwp](https://github.com/fkwp).
* Add `Close` tooltip to dialog ([#&#8203;28617](https://github.com/element-hq/element-web/pull/28617)). Contributed by [@&#8203;florianduros](https://github.com/florianduros).
* New UX for Share dialog ([#&#8203;28598](https://github.com/element-hq/element-web/pull/28598)). Contributed by [@&#8203;florianduros](https://github.com/florianduros).
* Improve performance of RoomContext in RoomHeader ([#&#8203;28574](https://github.com/element-hq/element-web/pull/28574)). Contributed by [@&#8203;t3chguy](https://github.com/t3chguy).
* Remove `Features.RustCrypto` flag ([#&#8203;28582](https://github.com/element-hq/element-web/pull/28582)). Contributed by [@&#8203;florianduros](https://github.com/florianduros).
* Add Modernizr warning when running in non-secure context ([#&#8203;28581](https://github.com/element-hq/element-web/pull/28581)). Contributed by [@&#8203;t3chguy](https://github.com/t3chguy).
* Fix jumpy timeline when the pinned message banner is displayed ([#&#8203;28654](https://github.com/element-hq/element-web/pull/28654)). Contributed by [@&#8203;florianduros](https://github.com/florianduros).
* Fix font & spaces in settings subsection ([#&#8203;28631](https://github.com/element-hq/element-web/pull/28631)). Contributed by [@&#8203;florianduros](https://github.com/florianduros).
* Remove manual device verification which is not supported by the new cryptography stack ([#&#8203;28588](https://github.com/element-hq/element-web/pull/28588)). Contributed by [@&#8203;florianduros](https://github.com/florianduros).
* Fix code block highlighting not working reliably with many code blocks ([#&#8203;28613](https://github.com/element-hq/element-web/pull/28613)). Contributed by [@&#8203;t3chguy](https://github.com/t3chguy).
* Remove remaining reply fallbacks code ([#&#8203;28610](https://github.com/element-hq/element-web/pull/28610)). Contributed by [@&#8203;t3chguy](https://github.com/t3chguy).

[1.9.47]
* Update element-web to 1.11.89
* [Full Changelog](https://github.com/element-hq/element-web/releases/tag/v1.11.89)
* Upgrade matrix-sdk-crypto-wasm to 1.11.0 (https://github.com/matrix-org/matrix-js-sdk/pull/4593)
* Fix url preview display ([#&#8203;28766](https://github.com/element-hq/element-web/pull/28766)).

[1.9.48]
* Update element-web to 1.11.90
* [Full Changelog](https://github.com/element-hq/element-web/releases/tag/v1.11.90)
* Docker: run as non-root ([#&#8203;28849](https://github.com/element-hq/element-web/pull/28849)). Contributed by [@&#8203;richvdh](https://github.com/richvdh).
* Docker: allow configuration of HTTP listen port via env var ([#&#8203;28840](https://github.com/element-hq/element-web/pull/28840)). Contributed by [@&#8203;richvdh](https://github.com/richvdh).
* Update matrix-wysiwyg to consume WASM asset ([#&#8203;28838](https://github.com/element-hq/element-web/pull/28838)). Contributed by [@&#8203;t3chguy](https://github.com/t3chguy).
* OIDC settings tweaks ([#&#8203;28787](https://github.com/element-hq/element-web/pull/28787)). Contributed by [@&#8203;t3chguy](https://github.com/t3chguy).
* Delabs native OIDC support ([#&#8203;28615](https://github.com/element-hq/element-web/pull/28615)). Contributed by [@&#8203;t3chguy](https://github.com/t3chguy).

[1.9.49]
* Update element-web to 1.11.91
* [Full Changelog](https://github.com/element-hq/element-web/releases/tag/v1.11.91)
* Implement changes to memberlist from feedback ([#&#8203;29029](https://github.com/element-hq/element-web/pull/29029)). Contributed by [@&#8203;MidhunSureshR](https://github.com/MidhunSureshR).
* Add toast for recovery keys being out of sync ([#&#8203;28946](https://github.com/element-hq/element-web/pull/28946)). Contributed by [@&#8203;dbkr](https://github.com/dbkr).
* Refactor LegacyCallHandler event emitter to use TypedEventEmitter ([#&#8203;29008](https://github.com/element-hq/element-web/pull/29008)). Contributed by [@&#8203;t3chguy](https://github.com/t3chguy).
* Add `Recovery` section in the new user settings `Encryption` tab ([#&#8203;28673](https://github.com/element-hq/element-web/pull/28673)). Contributed by [@&#8203;florianduros](https://github.com/florianduros).
* Retry loading chunks to make the app more resilient ([#&#8203;29001](https://github.com/element-hq/element-web/pull/29001)). Contributed by [@&#8203;t3chguy](https://github.com/t3chguy).
* Clear account idb table on logout ([#&#8203;28996](https://github.com/element-hq/element-web/pull/28996)). Contributed by [@&#8203;t3chguy](https://github.com/t3chguy).
* Implement new memberlist design with MVVM architecture  ([#&#8203;28874](https://github.com/element-hq/element-web/pull/28874)). Contributed by [@&#8203;MidhunSureshR](https://github.com/MidhunSureshR).
* \[Backport staging] Switch to secure random strings ([#&#8203;29035](https://github.com/element-hq/element-web/pull/29035)). Contributed by [@&#8203;RiotRobot](https://github.com/RiotRobot).
* React to MatrixEvent sender/target being updated for rendering state events ([#&#8203;28947](https://github.com/element-hq/element-web/pull/28947)). Contributed by [@&#8203;t3chguy](https://github.com/t3chguy).

[1.9.50]
* Update element-web to 1.11.92
* [Full Changelog](https://github.com/element-hq/element-web/releases/tag/v1.11.92)
* \[Backport staging] Log when we show, and hide, encryption setup toasts ([#&#8203;29238](https://github.com/element-hq/element-web/pull/29238)). Contributed by [@&#8203;richvdh](https://github.com/richvdh).
* Make profile header section match the designs ([#&#8203;29163](https://github.com/element-hq/element-web/pull/29163)). Contributed by [@&#8203;MidhunSureshR](https://github.com/MidhunSureshR).
* Always show back button in the right panel ([#&#8203;29128](https://github.com/element-hq/element-web/pull/29128)). Contributed by [@&#8203;MidhunSureshR](https://github.com/MidhunSureshR).
* Schedule dehydration on reload if the dehydration key is already cached locally ([#&#8203;29021](https://github.com/element-hq/element-web/pull/29021)). Contributed by [@&#8203;uhoreg](https://github.com/uhoreg).
* update to twemoji 15.1.0 ([#&#8203;29115](https://github.com/element-hq/element-web/pull/29115)). Contributed by [@&#8203;ara4n](https://github.com/ara4n).
* Update matrix-widget-api ([#&#8203;29112](https://github.com/element-hq/element-web/pull/29112)). Contributed by [@&#8203;toger5](https://github.com/toger5).
* Allow navigating through the memberlist using up/down keys ([#&#8203;28949](https://github.com/element-hq/element-web/pull/28949)). Contributed by [@&#8203;MidhunSureshR](https://github.com/MidhunSureshR).
* Style room header icons and facepile for toggled state ([#&#8203;28968](https://github.com/element-hq/element-web/pull/28968)). Contributed by [@&#8203;MidhunSureshR](https://github.com/MidhunSureshR).
* Move threads header below base card header ([#&#8203;28969](https://github.com/element-hq/element-web/pull/28969)). Contributed by [@&#8203;MidhunSureshR](https://github.com/MidhunSureshR).
* Add `Advanced` section to the user settings encryption tab ([#&#8203;28804](https://github.com/element-hq/element-web/pull/28804)). Contributed by [@&#8203;florianduros](https://github.com/florianduros).
* Fix outstanding UX issues with replies/mentions/keyword notifs ([#&#8203;28270](https://github.com/element-hq/element-web/pull/28270)). Contributed by [@&#8203;taffyko](https://github.com/taffyko).
* Distinguish room state and timeline events when dealing with widgets ([#&#8203;28681](https://github.com/element-hq/element-web/pull/28681)). Contributed by [@&#8203;robintown](https://github.com/robintown).
* Switch OIDC primarily to new `/auth_metadata` API ([#&#8203;29019](https://github.com/element-hq/element-web/pull/29019)). Contributed by [@&#8203;t3chguy](https://github.com/t3chguy).
* More memberlist changes ([#&#8203;29069](https://github.com/element-hq/element-web/pull/29069)). Contributed by [@&#8203;MidhunSureshR](https://github.com/MidhunSureshR).
* \[Backport staging] Wire up the "Forgot recovery key" button for the "Key storage out of sync" toast ([#&#8203;29190](https://github.com/element-hq/element-web/pull/29190)). Contributed by [@&#8203;RiotRobot](https://github.com/RiotRobot).
* Encryption tab: hide `Advanced` section when the key storage is out of sync ([#&#8203;29129](https://github.com/element-hq/element-web/pull/29129)). Contributed by [@&#8203;florianduros](https://github.com/florianduros).
* Fix share button in discovery settings being disabled incorrectly ([#&#8203;29151](https://github.com/element-hq/element-web/pull/29151)). Contributed by [@&#8203;t3chguy](https://github.com/t3chguy).
* Ensure switching rooms does not wrongly focus timeline search ([#&#8203;29153](https://github.com/element-hq/element-web/pull/29153)). Contributed by [@&#8203;t3chguy](https://github.com/t3chguy).
* Stop showing a dialog prompting the user to enter an old recovery key ([#&#8203;29143](https://github.com/element-hq/element-web/pull/29143)). Contributed by [@&#8203;richvdh](https://github.com/richvdh).
* Make themed widgets reflect the effective theme ([#&#8203;28342](https://github.com/element-hq/element-web/pull/28342)). Contributed by [@&#8203;robintown](https://github.com/robintown).
* support non-VS16 emoji ligatures in TwemojiMozilla ([#&#8203;29100](https://github.com/element-hq/element-web/pull/29100)). Contributed by [@&#8203;ara4n](https://github.com/ara4n).
* e2e test: Verify session with the encryption tab instead of the security & privacy tab ([#&#8203;29090](https://github.com/element-hq/element-web/pull/29090)). Contributed by [@&#8203;florianduros](https://github.com/florianduros).
* Work around cloudflare R2 / aws client incompatability ([#&#8203;29086](https://github.com/element-hq/element-web/pull/29086)). Contributed by [@&#8203;dbkr](https://github.com/dbkr).
* Fix identity server settings visibility ([#&#8203;29083](https://github.com/element-hq/element-web/pull/29083)). Contributed by [@&#8203;dbkr](https://github.com/dbkr).

[1.9.51]
* Update element-web to 1.11.93
* [Full Changelog](https://github.com/element-hq/element-web/releases/tag/v1.11.93)
* \[backport] Dynamically load Element Web modules in Docker entrypoint ([#&#8203;29358](https://github.com/element-hq/element-web/pull/29358)). Contributed by [@&#8203;t3chguy](https://github.com/t3chguy).
* ChangeRecoveryKey: error handling ([#&#8203;29262](https://github.com/element-hq/element-web/pull/29262)). Contributed by [@&#8203;richvdh](https://github.com/richvdh).
* Dehydration: enable dehydrated device on "Set up recovery" ([#&#8203;29265](https://github.com/element-hq/element-web/pull/29265)). Contributed by [@&#8203;richvdh](https://github.com/richvdh).
* Render reason for invite rejection. ([#&#8203;29257](https://github.com/element-hq/element-web/pull/29257)). Contributed by [@&#8203;Half-Shot](https://github.com/Half-Shot).
* New room list: add search section ([#&#8203;29251](https://github.com/element-hq/element-web/pull/29251)). Contributed by [@&#8203;florianduros](https://github.com/florianduros).
* New room list: hide favourites and people meta spaces ([#&#8203;29241](https://github.com/element-hq/element-web/pull/29241)). Contributed by [@&#8203;florianduros](https://github.com/florianduros).
* New Room List: Create new labs flag ([#&#8203;29239](https://github.com/element-hq/element-web/pull/29239)). Contributed by [@&#8203;MidhunSureshR](https://github.com/MidhunSureshR).
* Stop URl preview from covering message box ([#&#8203;29215](https://github.com/element-hq/element-web/pull/29215)). Contributed by [@&#8203;edent](https://github.com/edent).
* Rename "security key" into "recovery key" ([#&#8203;29217](https://github.com/element-hq/element-web/pull/29217)). Contributed by [@&#8203;florianduros](https://github.com/florianduros).

[1.9.52]
* Update element-web to 1.11.94
* [Full Changelog](https://github.com/element-hq/element-web/releases/tag/v1.11.94)
* \[Backport staging] fix: /tmp/element-web-config may already exist preventing the container from booting up ([#&#8203;29377](https://github.com/element-hq/element-web/pull/29377)). Contributed by [@&#8203;RiotRobot](https://github.com/RiotRobot).

[1.9.53]
* Update element-web to 1.11.95
* [Full Changelog](https://github.com/element-hq/element-web/releases/tag/v1.11.95)
* Room List Store: Filter rooms by active space ([#&#8203;29399](https://github.com/element-hq/element-web/pull/29399)). Contributed by [@&#8203;MidhunSureshR](https://github.com/MidhunSureshR).
* Room List - Update the room list store on actions from the dispatcher ([#&#8203;29397](https://github.com/element-hq/element-web/pull/29397)). Contributed by [@&#8203;MidhunSureshR](https://github.com/MidhunSureshR).
* Room List  - Implement a minimal view model ([#&#8203;29357](https://github.com/element-hq/element-web/pull/29357)). Contributed by [@&#8203;MidhunSureshR](https://github.com/MidhunSureshR).
* New room list: add space menu in room header ([#&#8203;29352](https://github.com/element-hq/element-web/pull/29352)). Contributed by [@&#8203;florianduros](https://github.com/florianduros).
* Room List - Store sorted rooms in skip list ([#&#8203;29345](https://github.com/element-hq/element-web/pull/29345)). Contributed by [@&#8203;MidhunSureshR](https://github.com/MidhunSureshR).
* New room list: add dial to search section ([#&#8203;29359](https://github.com/element-hq/element-web/pull/29359)). Contributed by [@&#8203;florianduros](https://github.com/florianduros).
* New room list: add compose menu for spaces in header ([#&#8203;29347](https://github.com/element-hq/element-web/pull/29347)). Contributed by [@&#8203;florianduros](https://github.com/florianduros).
* Use EditInPlace control for Identity Server picker to improve a11y ([#&#8203;29280](https://github.com/element-hq/element-web/pull/29280)). Contributed by [@&#8203;Half-Shot](https://github.com/Half-Shot).
* First step to add header to new room list ([#&#8203;29320](https://github.com/element-hq/element-web/pull/29320)). Contributed by [@&#8203;florianduros](https://github.com/florianduros).
* Add Windows 64-bit arm link and remove 32-bit link on compatibility page ([#&#8203;29312](https://github.com/element-hq/element-web/pull/29312)). Contributed by [@&#8203;t3chguy](https://github.com/t3chguy).
* Honour the backup disable flag from Element X ([#&#8203;29290](https://github.com/element-hq/element-web/pull/29290)). Contributed by [@&#8203;dbkr](https://github.com/dbkr).
* Fix edited code block width ([#&#8203;29394](https://github.com/element-hq/element-web/pull/29394)). Contributed by [@&#8203;florianduros](https://github.com/florianduros).
* new room list: keep space name in one line in header ([#&#8203;29369](https://github.com/element-hq/element-web/pull/29369)). Contributed by [@&#8203;florianduros](https://github.com/florianduros).
* Dismiss "Key storage out of sync" toast when secrets received ([#&#8203;29348](https://github.com/element-hq/element-web/pull/29348)). Contributed by [@&#8203;richvdh](https://github.com/richvdh).
* Minor CSS fixes for the new room list ([#&#8203;29334](https://github.com/element-hq/element-web/pull/29334)). Contributed by [@&#8203;florianduros](https://github.com/florianduros).
* Add padding to room header icon ([#&#8203;29271](https://github.com/element-hq/element-web/pull/29271)). Contributed by [@&#8203;langleyd](https://github.com/langleyd).

[1.10.0]
* Update base image to 5.0.0

