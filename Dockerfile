FROM cloudron/base:4.2.0@sha256:46da2fffb36353ef714f97ae8e962bd2c212ca091108d768ba473078319a47f4

RUN mkdir -p /app/code /app/pkg
WORKDIR /app/pkg

# renovate: datasource=github-releases depName=element-hq/element-web versioning=semver extractVersion=^v(?<version>.+)$
ARG ELEMENT_VERSION=1.11.95

# https://riot.im/develop/config.json has a sample config.json
RUN curl -L https://github.com/element-hq/element-web/releases/download/v${ELEMENT_VERSION}/element-v${ELEMENT_VERSION}.tar.gz | tar -xz -C /app/code --strip-components=1 && \
    ln -s /app/data/config.json /app/code/config.json && \
    ln -s /app/data/custom /app/code/custom

RUN yarn add tldjs
RUN yarn global add http-server

COPY config.json.template start.sh /app/pkg/

CMD [ "/app/pkg/start.sh" ]

