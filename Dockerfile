FROM cloudron/base:5.0.0@sha256:04fd70dbd8ad6149c19de39e35718e024417c3e01dc9c6637eaf4a41ec4e596c

RUN mkdir -p /app/code /app/pkg
WORKDIR /app/pkg

# renovate: datasource=github-releases depName=element-hq/element-web versioning=semver extractVersion=^v(?<version>.+)$
ARG ELEMENT_VERSION=1.11.95

# https://riot.im/develop/config.json has a sample config.json
RUN curl -L https://github.com/element-hq/element-web/releases/download/v${ELEMENT_VERSION}/element-v${ELEMENT_VERSION}.tar.gz | tar -xz -C /app/code --strip-components=1 && \
    ln -s /app/data/config.json /app/code/config.json && \
    ln -s /app/data/custom /app/code/custom

RUN npm install --no-update-notifier -g yarn && \
    yarn add tldjs --no-cache && \
    yarn global add http-server --no-cache

COPY config.json.template start.sh /app/pkg/

CMD [ "/app/pkg/start.sh" ]

