#!/bin/bash

set -eu

mkdir -p /app/data/custom

if [[ ! -f /app/data/config.json ]]; then
    echo "==> Creating configs on first run"
    sld=$(node -e "console.log(require('tldjs').getDomain('${CLOUDRON_APP_DOMAIN}'))")

    echo "==> Configuring element to use matrix.${sld} as the homeserver"

    # this is only setup on first run. '|' separates expressions. server_name is only for display purposes
    jq ".default_server_config[\"m.homeserver\"].base_url = \"https://matrix.${sld}\" | .default_server_config[\"m.homeserver\"].server_name = \"${sld}\"" /app/pkg/config.json.template | sponge /app/data/config.json
fi

chown -R cloudron:cloudron /app/data

echo "==> Starting Element"
exec /usr/local/bin/gosu cloudron:cloudron http-server /app/code -a 0.0.0.0 --port 3000 -d false

