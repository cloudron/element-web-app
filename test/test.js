#!/usr/bin/env node

/* jshint esversion: 8 */
/* global it, xit, describe, before, after, afterEach */

'use strict';

require('chromedriver');

const execSync = require('child_process').execSync,
    expect = require('expect.js'),
    fs = require('fs'),
    path = require('path'),
    { Builder, By, Key, until } = require('selenium-webdriver'),
    { Options } = require('selenium-webdriver/chrome');


if (!process.env.USERNAME || !process.env.PASSWORD) {
    console.log('USERNAME and PASSWORD env vars need to be set');
    process.exit(1);
}

describe('Application life cycle test', function () {
    this.timeout(0);

    const LOCATION = process.env.LOCATION || 'test';
    const MATRIX_LOCATION = 'matrix';
    const TEST_TIMEOUT = parseInt(process.env.TIMEOUT, 10) || 10000;
    const EXEC_ARGS = { cwd: path.resolve(__dirname, '..'), stdio: 'inherit' };
    const USERNAME = process.env.USERNAME;
    const PASSWORD = process.env.PASSWORD;
    const ROOM_ID = Math.floor((Math.random() * 100) + 1);
    const ROOM_NAME = 'Test room ' + ROOM_ID;
    const MSG_TEXT = 'Test message ';

    let browser, app;

    before(async function () {
        await startBrowser();
        if (!fs.existsSync('./screenshots')) fs.mkdirSync('./screenshots');

        if (process.env.CI) execSync(`cloudron uninstall --app ${MATRIX_LOCATION} || true`, EXEC_ARGS);
    });

    after(function () {
        browser.quit();
    });

    afterEach(async function () {
        if (!process.env.CI || !app) return;

        const currentUrl = await browser.getCurrentUrl();
        if (!currentUrl.includes(app.domain)) return;
        expect(this.currentTest.title).to.be.a('string');

        const screenshotData = await browser.takeScreenshot();
        fs.writeFileSync(`./screenshots/${new Date().getTime()}-${this.currentTest.title.replaceAll(' ', '_')}.png`, screenshotData, 'base64');
    });

    async function startBrowser() {
        browser = null;
        const chromeOptions = new Options().windowSize({ width: 1280, height: 1024 });
        if (process.env.CI) chromeOptions.addArguments('no-sandbox', 'disable-dev-shm-usage', 'headless');
        browser = new Builder().forBrowser('chrome').setChromeOptions(chromeOptions).build();
    }

    async function clearCache() {
        await browser.manage().deleteAllCookies();
        await browser.quit();
        await startBrowser();
    }

    function sleep(millis) {
        return new Promise(resolve => setTimeout(resolve, millis));
    }

    async function waitForElement(elem) {
        await browser.wait(until.elementLocated(elem), TEST_TIMEOUT);
        await browser.wait(until.elementIsVisible(browser.findElement(elem)), TEST_TIMEOUT);
    }

    function getAppInfo() {
        const inspect = JSON.parse(execSync('cloudron inspect'));
        app = inspect.apps.filter(function (a) { return a.location === LOCATION || a.location === LOCATION+2; })[0];
        expect(app).to.be.an('object');
    }

    function getMessage() {
        return MSG_TEXT + Math.floor((Math.random() * 100) + 1);
    }

    async function updateSynapseConfig() {
        console.log(`Setting Synapse Matrix server location to "https://${app.fqdn}"`);
        execSync(`cloudron exec --app ${ELEMENT_LOCATION} -- bash -c "jq '.default_server_config[\\"m.homeserver\\"].base_url = \\"https://${app.fqdn}\\"' /app/data/config.json | sponge /app/data/config.json"`);
        execSync(`cloudron restart --app ${ELEMENT_LOCATION}`);
        // wait when all services are up and running
        await sleep(15000);
    }

    async function registerUser() {
        await browser.get(`https://${app.fqdn}/#/register`);
        await browser.sleep(10000);
        await waitForElement(By.xpath('//input[@label="Username"]'));
        await browser.findElement(By.xpath('//input[@label="Username"]')).sendKeys(USERNAME);
        await browser.sleep(2000);
        await browser.findElement(By.xpath('//input[@label="Password"]')).sendKeys(PASSWORD);
        await browser.sleep(2000);
        await browser.findElement(By.xpath('//input[@label="Confirm password"]')).sendKeys(PASSWORD);
        await browser.sleep(2000);
        await browser.findElement(By.xpath('//input[@value="Register"]')).click();
        await browser.sleep(2000);

        await waitForElement(By.xpath('//h1[text()="You\'re in" or contains(., "Welcome")]'));
        if (await browser.findElements(By.xpath(`//h1[text()="You\'re in"]`)).then(found => !!found.length)) {
            await browser.findElement(By.xpath('//div[@role="button" and text()="Skip"]')).click();
            await browser.sleep(2000);
        }
        await waitForElement(By.xpath(`//h1[contains(., "Welcome")]`));
    }

    async function loginOIDC(username, password, alreadyAuthenticated = true) {
        browser.manage().deleteAllCookies();
        await browser.get(`https://${app.fqdn}/#/login`);
        await browser.sleep(6000);

        await waitForElement(By.xpath('//div[@role="button" and contains(., "Continue with")]'));
        await browser.findElement(By.xpath('//div[@role="button" and contains(., "Continue with")]')).click();
        await browser.sleep(2000);

        if (!alreadyAuthenticated) {
            await waitForElement(By.id('inputUsername'));
            await browser.findElement(By.id('inputUsername')).sendKeys(username);
            await browser.findElement(By.id('inputPassword')).sendKeys(password);
            await browser.findElement(By.id('loginSubmitButton')).click();
        }

        await waitForElement(By.xpath('//p[@class="confirm-trust" and contains(., "Continuing will grant ")]'));
        await browser.findElement(By.xpath('//a[contains(., "Continue")]')).click();
        await browser.sleep(2000);

        if (await browser.findElements(By.xpath('//div[@aria-label="Skip verification for now"]')).then(found => !!found.length)) {
            await skipVerification();
        }

        await browser.sleep(2000);
        await waitForElement(By.xpath(`//h1[contains(., "Welcome")] | //div[@role="button" and text()="Reset all"] | //div[text()="Proceed with reset"]`));
        if (await browser.findElements(By.xpath('//div[@role="button" and text()="Reset all"]')).then(found => !!found.length)) {
            await browser.findElement(By.xpath('//div[@role="button" and text()="Reset all"]')).click();
        }

        await waitForElement(By.xpath(`//h1[contains(., "Welcome")] | //div[text()="Proceed with reset"]`));
        if (await browser.findElements(By.xpath('//div[text()="Proceed with reset"]')).then(found => !!found.length)) {
            await browser.findElement(By.xpath('//div[text()="Proceed with reset"]')).click();

            await waitForElement(By.xpath('//div[@class="mx_Dialog_buttons"]//button[text()="Continue"]'));
            await browser.findElement(By.xpath('//div[@class="mx_Dialog_buttons"]//button[text()="Continue"]')).click();

            await waitForElement(By.xpath('//div[text()="Copy"]'));
            await browser.findElement(By.xpath('//div[text()="Copy"]')).click();

            await browser.sleep(1000);

            await waitForElement(By.xpath('//div[@class="mx_Dialog_buttons"]//button[text()="Continue"]'));
            await browser.findElement(By.xpath('//div[@class="mx_Dialog_buttons"]//button[text()="Continue"]')).click();

            await waitForElement(By.xpath('//button[text()="Done"] | //div[@role="button" and text()="Single Sign On"]'));
            if (await browser.findElements(By.xpath('//div[@role="button" and text()="Single Sign On"]')).then(found => !!found.length)) {
                await browser.findElement(By.xpath('//div[@role="button" and text()="Single Sign On"]')).click();
                // confirm on a new tab
                const originalWindowHandle = await browser.getWindowHandle();
                await browser.wait(async () => (await browser.getAllWindowHandles()).length === 2, 10000);
                //Loop through until we find a new window handle
                const windows = await browser.getAllWindowHandles();
                windows.forEach(async handle => {
                    if (handle !== originalWindowHandle) {
                        await browser.switchTo().window(handle);
                    }
                });
                await waitForElement(By.xpath('//a[contains(., "Continue with")]'));
                await browser.findElement(By.xpath('//a[contains(., "Continue with")]')).click();

                // switch back to the main window
                await browser.switchTo().window(originalWindowHandle);

                await waitForElement(By.xpath('//div[@role="button" and text()="Confirm"]'));
                await browser.findElement(By.xpath('//div[@role="button" and text()="Confirm"]')).click();
                await browser.sleep(2000);

                await waitForElement(By.xpath('//button[text()="Done"]'));
                await browser.findElement(By.xpath('//button[text()="Done"]')).click();

            } else {
                await browser.findElement(By.xpath('//button[text()="Done"]')).click();
            }
        }

        await browser.sleep(3000);
        await waitForElement(By.xpath(`//h1[contains(., "Welcome")]`));
    }

    async function login() {
        await browser.get('https://' + app.fqdn + '/#/login');
        await browser.wait(until.elementLocated(By.xpath('//input[@value="Sign in"]')), TEST_TIMEOUT);
        await browser.findElement(By.xpath('//input[@name="username"]')).sendKeys(USERNAME);
        await browser.findElement(By.xpath('//input[@name="password"]')).sendKeys(PASSWORD);
        await browser.findElement(By.xpath('//input[@value="Sign in"]')).click();
        await browser.sleep(5000);

        if (await browser.findElements(By.xpath('//div[@aria-label="Skip verification for now"]')).then(found => !!found.length)) {
            await skipVerification();
        }

        await browser.wait(until.elementLocated(By.xpath('//span[text()="Rooms"]')), TEST_TIMEOUT);
    }

    async function skipVerification() {
        await browser.wait(until.elementLocated(By.xpath('//div[@aria-label="Skip verification for now"]')), TEST_TIMEOUT);
        await browser.sleep(5000);
        await browser.findElement(By.xpath('//div[@aria-label="Skip verification for now"]')).click();
        await browser.wait(until.elementLocated(By.xpath('//div[contains(text(), "verify later")]')), TEST_TIMEOUT);
        await browser.sleep(5000);
        await browser.findElement(By.xpath('//div[contains(text(), "verify later")]')).click();
        await browser.sleep(5000);
    }

    async function logout() {
/*
        await browser.get('https://' + app.fqdn + '/#/home');
        await browser.sleep(5000);
        await waitForElement(By.xpath('//div[@class="mx_UserMenu"]'));
        await browser.findElement(By.xpath('//div[@class="mx_UserMenu"]')).click();

        await waitForElement(By.xpath('//li[@role="menuitem" and @aria-label="Sign out"]'));
        await browser.findElement(By.xpath('//li[@role="menuitem" and @aria-label="Sign out"]')).click();
        await browser.sleep(2000);

        if (await browser.findElements(By.xpath('//button[contains(text(), "I don\'t want my encrypted messages")]')).then(found => !!found.length)) {
            await browser.findElement(By.xpath('//button[contains(text(), "I don\'t want my encrypted messages")]')).click();
            await browser.sleep(3000);
        }

        await waitForElement(By.xpath('//h1[text()="Sign in"]'));
*/
        await clearCache();
    }

    async function isLoggedIn() {
        await browser.get('https://' + app.fqdn + '/#/home');
        await browser.wait(until.elementLocated(By.xpath('//span[text()="Rooms"]')), TEST_TIMEOUT);
    }

    async function createRoom() {
        await browser.get('https://' + app.fqdn + '/#/home');
        await browser.sleep(4000);
        await waitForElement(By.xpath('//div[@role="button" and @aria-label="Add room"]'));
        await browser.findElement(By.xpath('//div[@role="button" and @aria-label="Add room"]')).click();
        await browser.sleep(2000);
        await browser.findElement(By.xpath('//li[@role="menuitem" and @aria-label="New room"]')).click();
        await browser.sleep(2000);

        await browser.findElement(By.xpath('//input[@label="Name"]')).sendKeys(ROOM_NAME);
        await browser.sleep(2000);

        await browser.findElement(By.xpath('//button[text()="Create room"]')).click();
        await browser.sleep(2000);

        await waitForElement(By.xpath('//div[@role="button" and @aria-label="Add room"]'));

        await waitForElement(By.xpath('//div[@class="mx_RoomTile_titleContainer"]/div[@title="' + ROOM_NAME + '"]'));
    }

    async function checkRoom() {
        await browser.get('https://' + app.fqdn + '/#/home');
        await browser.sleep(4000);
        await waitForElement(By.xpath('//div[@role="treeitem" and @aria-label="' + ROOM_NAME + '"]'));
        await browser.findElement(By.xpath('//div[@role="treeitem" and @aria-label="' + ROOM_NAME + '"]')).click();
        await browser.sleep(2000);
        await waitForElement(By.xpath('//h2[text()="' + ROOM_NAME + '"]'));
    }

    async function sendMessage() {
        await checkRoom();

        await browser.findElement(By.xpath('//div[contains(@class, "mx_BasicMessageComposer_input")]')).sendKeys(getMessage());
        await browser.sleep(2000);

        await browser.findElement(By.xpath('//div[@role="button" and @aria-label="Send message"]')).click();
        await browser.sleep(2000);
    }


    xit('build app', function () { execSync('cloudron build', EXEC_ARGS); });

    // No SSO
    it('install matrix (no sso)', function () { execSync(`cloudron install --no-sso --appstore-id org.matrix.synapse --location ${MATRIX_LOCATION}`, EXEC_ARGS); });

    it('install app', async function () {
        execSync('cloudron install --location ' + LOCATION, EXEC_ARGS);
        await browser.sleep(10000);
    });
    it('can get app information', getAppInfo);

    it('can register new user', registerUser);
    it('create room', createRoom);
    it('can send message', sendMessage);

    it('can logout', logout);

    it('move to different location', async function () {
        browser.manage().deleteAllCookies();
        await browser.get('about:blank');

        execSync(`cloudron configure --location ${LOCATION}2`, EXEC_ARGS);
        getAppInfo();
    });

    it('can login', login);
    it('check room', checkRoom);
    it('can logout', logout);

    it('uninstall app', async function () {
        await browser.get('about:blank');
        execSync(`cloudron uninstall --app ${app.id}`, EXEC_ARGS);
    });
    it('uninstall app', function () { execSync(`cloudron uninstall --app ${MATRIX_LOCATION}`, EXEC_ARGS); });

    // SSO
    it('install matrix', function () { execSync(`cloudron install --appstore-id org.matrix.synapse --location ${MATRIX_LOCATION}`, EXEC_ARGS); });
    it('install app', function () { execSync('cloudron install --location ' + LOCATION, EXEC_ARGS); });
    it('can get app info', getAppInfo);

    it('can login via OIDC', loginOIDC.bind(null, USERNAME, PASSWORD, false));
    it('create room', createRoom);
    it('can send message', sendMessage);

    it('can restart app', function () { execSync(`cloudron restart --app ${app.id}`); });

    it('is logged in', isLoggedIn);

    it('backup app', function () { execSync(`cloudron backup create --app ${app.id}`, EXEC_ARGS); });

    it('is logged in', isLoggedIn);

    it('restore app', async function () {
        const backups = JSON.parse(execSync(`cloudron backup list --raw --app ${app.id}`));

        await browser.get('about:blank');
        execSync(`cloudron uninstall --app ${app.id}`, EXEC_ARGS);
        execSync(`cloudron install --location ${LOCATION}`, EXEC_ARGS);

        getAppInfo();

        execSync(`cloudron restore --backup ${backups[0].id} --app ${app.id}`, EXEC_ARGS);

        await browser.sleep(10000);
    });

    it('is logged in', isLoggedIn);
    it('check room', checkRoom);
    it('can send message', sendMessage);
    it('can logout', logout);

    it('can login via OIDC', loginOIDC.bind(null, USERNAME, PASSWORD, false));
    it('check room', checkRoom);
    it('can send message', sendMessage);
    it('can logout', logout);

    it('uninstall app', async function () {
        await browser.get('about:blank');
        execSync(`cloudron uninstall --app ${app.id}`, EXEC_ARGS);
    });
    it('uninstall app', function () { execSync(`cloudron uninstall --app ${MATRIX_LOCATION}`, EXEC_ARGS); });

    // test update
    it('install matrix', function () { execSync(`cloudron install --appstore-id org.matrix.synapse --location ${MATRIX_LOCATION}`, EXEC_ARGS); });
    it('can install app', function () { execSync('cloudron install --appstore-id im.riot.cloudronapp --location ' + LOCATION, EXEC_ARGS); });
    it('can get app info', getAppInfo);

    it('can login via OIDC', loginOIDC.bind(null, USERNAME, PASSWORD, false));
    it('create room', createRoom);
    it('can send message', sendMessage);
    it('can logout', logout);

    it('can update', function () { execSync(`cloudron update --app ${app.id}`, EXEC_ARGS); });

    it('can login via OIDC', loginOIDC.bind(null, USERNAME, PASSWORD, false));
    it('is logged in', isLoggedIn);
    it('check room', checkRoom);
    it('can send message', sendMessage);

    it('uninstall app', async function () {
        await browser.get('about:blank');
        execSync(`cloudron uninstall --app ${app.id}`, EXEC_ARGS);
    });

    it('uninstall matrix', function () {
        execSync(`cloudron uninstall --app ${MATRIX_LOCATION}`, EXEC_ARGS);
    });
});
